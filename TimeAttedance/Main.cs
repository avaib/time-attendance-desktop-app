﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.WebService;

namespace TimeAttedance
{
    class Main
    {
      public string[] TimeZone = { "(UTC+00.00) Western Europe Time, London, Lisbon, Casablanca, Monrovia",
                                        "(UTC-12.00) Eniwetok, Kwajalein",
                                        "(UTC-11.00) Midway Island, Samoa",
                                        "(UTC-10.00) Hawaii",
                                        "(UTC-09.00) Alaska",
                                        "(UTC-08.00) Pacific Time (US &amp; Canada)",
                                        "(UTC-07.00) Mountain Time (US &amp; Canada)",
                                        "(UTC-06.00) Central Time (US &amp; Canada), Mexico City",
                                        "(UTC-05.00) Eastern Time (US &amp; Canada), Bogota, Lima, Quito",
                                        "(UTC-04.00) Atlantic Time (Canada), Caracas, La Paz",
                                        "(UTC-03.50) Newfoundland",
                                        "(UTC-03.00) Brazil, Buenos Aires, Georgetown",
                                        "(UTC-02.00) Mid-Atlantic",
                                        "(UTC-01.00) Azores, Cape Verde Islands",
                                        "(UTC+01.00) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris",
                                        "(UTC+02.00) EET(Eastern Europe Time), Kaliningrad, South Africa",
                                        "(UTC+03.00) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi",
                                        "(UTC+03.50) Tehran",
                                        "(UTC+04.00) Abu Dhabi, Muscat, Baku, Tbilisi",
                                        "(UTC+04.50) Kabul",
                                        "(UTC+05.00) Ekaterinburg, Islamabad, Karachi, Tashkent",
                                        "(UTC+05.50) Bombay, Calcutta, Madras, New Delhi",
                                        "(UTC+06.00) Almaty, Dhaka, Colombo",
                                        "(UTC+07.00) Bangkok, Hanoi, Jakarta",
                                        "(UTC+08.00) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei",
                                        "(UTC+09.00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
                                        "(UTC+09.50) Adelaide, Darwin",
                                        "(UTC+10.00) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok",
                                        "(UTC+11.00) Magadan, Solomon Islands, New Caledonia",
                                        "(UTC+12.00) Auckland, Wellington, Fiji, Kamchatka, Marshall Island",
                                        "(UTC+13.00) Nuku'alofa"};


        string[] myDateformats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
                         "M/d/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                         "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                         "M/d/yyyy h:mm", "M/d/yyyy h:mm",
                         "M/d/yyyy hh:mm", "M/d/yyyy hh:mm",
                         "M/d/yyyy HH:mm:ss.ffffff" , "M/d/yyyy hh:mm:ss tt",


                         "MM/dd/yyyy h:mm:ss tt", "MM/dd/yyyy h:mm tt",
                         "MM/dd/yyyy hh:mm:ss", "MM/dd/yyyy h:mm:ss",
                         "MM/dd/yyyy hh:mm tt", "MM/dd/yyyy hh tt",
                         "MM/dd/yyyy h:mm", "MM/dd/yyyy h:mm",
                         "MM/dd/yyyy hh:mm", "MM/dd/yyyy hh:mm",
                         "MM/dd/yyyy HH:mm:ss.ffffff","MM/dd/yyyy hh:mm:ss tt",

                         "d/M/yyyy h:mm:ss tt", "d/M/yyyy h:mm tt",
                         "d/M/yyyy hh:mm:ss", "d/M/yyyy h:mm:ss",
                         "d/M/yyyy hh:mm tt", "d/M/yyyy hh tt",
                         "d/M/yyyy h:mm", "d/M/yyyy h:mm",
                         "d/M/yyyy hh:mm", "d/M/yyyy hh:mm",
                         "d/M/yyyy HH:mm:ss.ffffff","d/M/yyyy hh:mm:ss tt",

                         "dd/MM/yyyy h:mm:ss tt", "dd/MM/yyyy h:mm tt",
                         "dd/MM/yyyy hh:mm:ss", "dd/MM/yyyy h:mm:ss",
                         "dd/MM/yyyy hh:mm tt", "dd/MM/yyyy hh tt",
                         "dd/MM/yyyy h:mm", "dd/MM/yyyy h:mm",
                         "dd/MM/yyyy hh:mm", "dd/MM/yyyy hh:mm",
                         "dd/MM/yyyy HH:mm:ss.ffffff","dd/MM/yyyy hh:mm:ss tt",

                         "yyyy/M/d h:mm:ss tt", "yyyy/M/d h:mm tt",
                         "yyyy/M/d hh:mm:ss", "yyyy/M/d h:mm:ss",
                         "yyyy/M/d hh:mm tt", "yyyy/M/d hh tt",
                         "yyyy/M/d h:mm", "yyyy/M/d h:mm",
                         "yyyy/M/d hh:mm", "yyyy/M/d hh:mm",
                         "yyyy/M/d HH:mm:ss.ffffff","yyyy/M/d hh:mm:ss tt",

                         "yyyy/MM/dd h:mm:ss tt", "yyyy/MM/dd h:mm tt",
                         "yyyy/MM/dd hh:mm:ss", "yyyy/MM/dd h:mm:ss",
                         "yyyy/MM/dd hh:mm tt", "yyyy/MM/dd hh tt",
                         "yyyy/MM/dd h:mm", "yyyy/MM/dd h:mm",
                         "yyyy/MM/dd hh:mm", "yyyy/MM/dd hh:mm",
                         "yyyy/MM/dd HH:mm:ss.ffffff","yyyy/MM/dd hh:mm:ss tt",

                         "M-d-yyyy h:mm:ss tt", "M-d-yyyy h:mm tt",
                         "M-d-yyyy hh:mm:ss", "M-d-yyyy h:mm:ss",
                         "M-d-yyyy hh:mm tt", "M-d-yyyy hh tt",
                         "M-d-yyyy h:mm", "M-d-yyyy h:mm",
                         "M-d-yyyy hh:mm", "M-d-yyyy hh:mm",
                         "M-d-yyyy HH:mm:ss.ffffff","M-d-yyyy hh:mm:ss tt",

                         "MM-dd-yyyy h:mm:ss tt", "MM-dd-yyyy h:mm tt",
                         "MM-dd-yyyy hh:mm:ss", "MM-dd-yyyy h:mm:ss",
                         "MM-dd-yyyy hh:mm tt", "MM-dd-yyyy hh tt",
                         "MM-dd-yyyy h:mm", "MM-dd-yyyy h:mm",
                         "MM-dd-yyyy hh:mm", "MM-dd-yyyy hh:mm",
                         "MM-dd-yyyy HH:mm:ss.ffffff","MM-dd-yyyy hh:mm:ss tt",

                         "d-M-yyyy h:mm:ss tt", "d-M-yyyy h:mm tt",
                         "d-M-yyyy hh:mm:ss", "d-M-yyyy h:mm:ss",
                         "d-M-yyyy hh:mm tt", "d-M-yyyy hh tt",
                         "d-M-yyyy h:mm", "d-M-yyyy h:mm",
                         "d-M-yyyy hh:mm", "d-M-yyyy hh:mm",
                         "d-M-yyyy HH:mm:ss.ffffff","d-M-yyyy hh:mm:ss tt",

                         "dd-MM-yyyy h:mm:ss tt", "dd-MM-yyyy h:mm tt",
                         "dd-MM-yyyy hh:mm:ss", "dd-MM-yyyy h:mm:ss",
                         "dd-MM-yyyy hh:mm tt", "dd-MM-yyyy hh tt",
                         "dd-MM-yyyy h:mm", "dd-MM-yyyy h:mm",
                         "dd-MM-yyyy hh:mm", "dd-MM-yyyy hh:mm",
                         "dd-MM-yyyy HH:mm:ss.ffffff","dd-MM-yyyy hh:mm:ss tt",

                         "yyyy-M-d h:mm:ss tt", "yyyy-M-d h:mm tt",
                         "yyyy-M-d hh:mm:ss", "yyyy-M-d h:mm:ss",
                         "yyyy-M-d hh:mm tt", "yyyy-M-d hh tt",
                         "yyyy-M-d h:mm", "yyyy-M-d h:mm",
                         "yyyy-M-d hh:mm", "yyyy-M-d hh:mm",
                         "yyyy-M-d HH:mm:ss.ffffff","yyyy-M-d hh:mm:ss tt",

                         "yyyy-MM-dd h:mm:ss tt", "yyyy-MM-dd h:mm tt",
                         "yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd h:mm:ss",
                         "yyyy-MM-dd hh:mm tt", "yyyy-MM-dd hh tt",
                         "yyyy-MM-dd h:mm", "yyyy-MM-dd h:mm",
                         "yyyy-MM-dd hh:mm", "yyyy-MM-dd hh:mm",
                         "yyyy-MM-dd HH:mm:ss.ffffff","yyyy-MM-dd hh:mm:ss tt",

                         "M.d.yyyy h:mm:ss tt", "M.d.yyyy h:mm tt",
                         "M.d.yyyy hh:mm:ss", "M.d.yyyy h:mm:ss",
                         "M.d.yyyy hh:mm tt", "M.d.yyyy hh tt",
                         "M.d.yyyy h:mm", "M.d.yyyy h:mm",
                         "M.d.yyyy hh:mm", "M.d.yyyy hh:mm",
                         "M.d.yyyy HH:mm:ss.ffffff","M.d.yyyy hh:mm:ss tt",

                         "MM.dd.yyyy h:mm:ss tt", "MM.dd.yyyy h:mm tt",
                         "MM.dd.yyyy hh:mm:ss", "MM.dd.yyyy h:mm:ss",
                         "MM.dd.yyyy hh:mm tt", "MM.dd.yyyy hh tt",
                         "MM.dd.yyyy h:mm", "MM.dd.yyyy h:mm",
                         "MM.dd.yyyy hh:mm", "MM.dd.yyyy hh:mm",
                         "MM.dd.yyyy HH:mm:ss.ffffff","MM.dd.yyyy hh:mm:ss tt",

                         "d.M.yyyy h:mm:ss tt", "d.M.yyyy h:mm tt",
                         "d.M.yyyy hh:mm:ss", "d.M.yyyy h:mm:ss",
                         "d.M.yyyy hh:mm tt", "d.M.yyyy hh tt",
                         "d.M.yyyy h:mm", "d.M.yyyy h:mm",
                         "d.M.yyyy hh:mm", "d.M.yyyy hh:mm",
                         "d.M.yyyy HH:mm:ss.ffffff","d.M.yyyy hh:mm:ss tt",

                         "dd.MM.yyyy h:mm:ss tt", "dd.MM.yyyy h:mm tt",
                         "dd.MM.yyyy hh:mm:ss", "dd.MM.yyyy h:mm:ss",
                         "dd.MM.yyyy hh:mm tt", "dd.MM.yyyy hh tt",
                         "dd.MM.yyyy h:mm", "dd.MM.yyyy h:mm",
                         "dd.MM.yyyy hh:mm", "dd.MM.yyyy hh:mm",
                         "dd.MM.yyyy HH:mm:ss.ffffff","dd.MM.yyyy hh:mm:ss tt",

                         "yyyy.M.d h:mm:ss tt", "yyyy.M.d h:mm tt",
                         "yyyy.M.d hh:mm:ss", "yyyy.M.d h:mm:ss",
                         "yyyy.M.d hh:mm tt", "yyyy.M.d hh tt",
                         "yyyy.M.d h:mm", "yyyy.M.d h:mm",
                         "yyyy.M.d hh:mm", "yyyy.M.d hh:mm",
                         "yyyy.M.d HH:mm:ss.ffffff","yyyy.M.d hh:mm:ss tt",

                         "yyyy.MM.dd h:mm:ss tt", "yyyy.MM.dd h:mm tt",
                         "yyyy.MM.dd hh:mm:ss", "yyyy.MM.dd h:mm:ss",
                         "yyyy.MM.dd hh:mm tt", "yyyy.MM.dd hh tt",
                         "yyyy.MM.dd h:mm", "yyyy.MM.dd h:mm",
                         "yyyy.MM.dd hh:mm", "yyyy.MM.dd hh:mm",
                         "yyyy.MM.dd HH:mm:ss.ffffff","yyyy.MM.dd hh:mm:ss tt",

                         "M,d,yyyy h:mm:ss tt", "M,d,yyyy h:mm tt",
                         "M,d,yyyy hh:mm:ss", "M,d,yyyy h:mm:ss",
                         "M,d,yyyy hh:mm tt", "M,d,yyyy hh tt",
                         "M,d,yyyy h:mm", "M,d,yyyy h:mm",
                         "M,d,yyyy hh:mm", "M,d,yyyy hh:mm",
                         "M,d,yyyy HH:mm:ss.ffffff","M,d,yyyy hh:mm:ss tt",

                         "MM,dd,yyyy h:mm:ss tt", "MM,dd,yyyy h:mm tt",
                         "MM,dd,yyyy hh:mm:ss", "MM,dd,yyyy h:mm:ss",
                         "MM,dd,yyyy hh:mm tt", "MM,dd,yyyy hh tt",
                         "MM,dd,yyyy h:mm", "MM,dd,yyyy h:mm",
                         "MM,dd,yyyy hh:mm", "MM,dd,yyyy hh:mm",
                         "MM,dd,yyyy HH:mm:ss.ffffff","MM,dd,yyyy hh:mm:ss tt",

                         "d,M,yyyy h:mm:ss tt", "d,M,yyyy h:mm tt",
                         "d,M,yyyy hh:mm:ss", "d,M,yyyy h:mm:ss",
                         "d,M,yyyy hh:mm tt", "d,M,yyyy hh tt",
                         "d,M,yyyy h:mm", "d,M,yyyy h:mm",
                         "d,M,yyyy hh:mm", "d,M,yyyy hh:mm",
                         "d,M,yyyy HH:mm:ss.ffffff","d,M,yyyy hh:mm:ss tt",

                         "dd,MM,yyyy h:mm:ss tt", "dd,MM,yyyy h:mm tt",
                         "dd,MM,yyyy hh:mm:ss", "dd,MM,yyyy h:mm:ss",
                         "dd,MM,yyyy hh:mm tt", "dd,MM,yyyy hh tt",
                         "dd,MM,yyyy h:mm", "dd,MM,yyyy h:mm",
                         "dd,MM,yyyy hh:mm", "dd,MM,yyyy hh:mm",
                         "dd,MM,yyyy HH:mm:ss.ffffff","dd,MM,yyyy hh:mm:ss tt",

                         "yyyy,M,d h:mm:ss tt", "yyyy,M,d h:mm tt",
                         "yyyy,M,d hh:mm:ss", "yyyy,M,d h:mm:ss",
                         "yyyy,M,d hh:mm tt", "yyyy,M,d hh tt",
                         "yyyy,M,d h:mm", "yyyy,M,d h:mm",
                         "yyyy,M,d hh:mm", "yyyy,M,d hh:mm",
                         "yyyy,M,d HH:mm:ss.ffffff","yyyy,M,d hh:mm:ss tt",

                         "yyyy,MM,dd h:mm:ss tt", "yyyy,MM,dd h:mm tt",
                         "yyyy,MM,dd hh:mm:ss", "yyyy,MM,dd h:mm:ss",
                         "yyyy,MM,dd hh:mm tt", "yyyy,MM,dd hh tt",
                         "yyyy,MM,dd h:mm", "yyyy,MM,dd h:mm",
                         "yyyy,MM,dd hh:mm", "yyyy,MM,dd hh:mm",
                         "yyyy,MM,dd HH:mm:ss.ffffff","yyyy,MM,dd hh:mm:ss tt",

                         "M d yyyy h:mm:ss tt", "M d yyyy h:mm tt",
                         "M d yyyy hh:mm:ss", "M d yyyy h:mm:ss",
                         "M d yyyy hh:mm tt", "M d yyyy hh tt",
                         "M d yyyy h:mm", "M d yyyy h:mm",
                         "M d yyyy hh:mm", "M d yyyy hh:mm",
                         "M d yyyy HH:mm:ss.ffffff","M d yyyy hh:mm:ss tt",

                         "MM dd yyyy h:mm:ss tt", "MM dd yyyy h:mm tt",
                         "MM dd yyyy hh:mm:ss", "MM dd yyyy h:mm:ss",
                         "MM dd yyyy hh:mm tt", "MM dd yyyy hh tt",
                         "MM dd yyyy h:mm", "MM dd yyyy h:mm",
                         "MM dd yyyy hh:mm", "MM dd yyyy hh:mm",
                         "MM dd yyyy HH:mm:ss.ffffff","MM dd yyyy hh:mm:ss tt",

                         "d M yyyy h:mm:ss tt", "d M yyyy h:mm tt",
                         "d M yyyy hh:mm:ss", "d M yyyy h:mm:ss",
                         "d M yyyy hh:mm tt", "d M yyyy hh tt",
                         "d M yyyy h:mm", "d M yyyy h:mm",
                         "d M yyyy hh:mm", "d M yyyy hh:mm",
                         "d M yyyy HH:mm:ss.ffffff","d M yyyy hh:mm:ss tt",

                         "dd MM yyyy h:mm:ss tt", "dd MM yyyy h:mm tt",
                         "dd MM yyyy hh:mm:ss", "dd MM yyyy h:mm:ss",
                         "dd MM yyyy hh:mm tt", "dd MM yyyy hh tt",
                         "dd MM yyyy h:mm", "dd MM yyyy h:mm",
                         "dd MM yyyy hh:mm", "dd MM yyyy hh:mm",
                         "dd MM yyyy HH:mm:ss.ffffff","dd MM yyyy hh:mm:ss tt",

                         "yyyy M d h:mm:ss tt", "yyyy M d h:mm tt",
                         "yyyy M d hh:mm:ss", "yyyy M d h:mm:ss",
                         "yyyy M d hh:mm tt", "yyyy M d hh tt",
                         "yyyy M d h:mm", "yyyy M d h:mm",
                         "yyyy M d hh:mm", "yyyy M d hh:mm",
                         "yyyy M d HH:mm:ss.ffffff","yyyy M d hh:mm:ss tt",

                         "yyyy MM dd h:mm:ss tt", "yyyy MM dd h:mm tt",
                         "yyyy MM dd hh:mm:ss", "yyyy MM dd h:mm:ss",
                         "yyyy MM dd hh:mm tt", "yyyy MM dd hh tt",
                         "yyyy MM dd h:mm", "yyyy MM dd h:mm",
                         "yyyy MM dd hh:mm", "yyyy MM dd hh:mm",
                         "yyyy MM dd HH:mm:ss.ffffff","yyyy MM dd hh:mm:ss tt",

                         "M/dd/yyyy h:mm:ss tt", "M/dd/yyyy h:mm tt",
                         "M/dd/yyyy hh:mm:ss", "M/dd/yyyy h:mm:ss",
                         "M/dd/yyyy hh:mm tt", "M/dd/yyyy hh tt",
                         "M/dd/yyyy h:mm", "M/dd/yyyy h:mm",
                         "M/dd/yyyy hh:mm", "M/dd/yyyy hh:mm",
                         "M/dd/yyyy HH:mm:ss.ffffff","M/dd/yyyy hh:mm:ss tt",

                         "MM/d/yyyy h:mm:ss tt", "MM/d/yyyy h:mm tt",
                         "MM/d/yyyy hh:mm:ss", "MM/d/yyyy h:mm:ss",
                         "MM/d/yyyy hh:mm tt", "MM/d/yyyy hh tt",
                         "MM/d/yyyy h:mm", "MM/d/yyyy h:mm",
                         "MM/d/yyyy hh:mm", "MM/d/yyyy hh:mm",
                         "MM/d/yyyy HH:mm:ss.ffffff","MM/d/yyyy hh:mm:ss tt",

                         "dd-MMM-yyyy h:mm:ss tt", "dd-MMM-yyyy h:mm tt",
                         "dd-MMM-yyyy hh:mm:ss", "dd-MMM-yyyy h:mm:ss",
                         "dd-MMM-yyyy hh:mm tt", "dd-MMM-yyyy hh tt",
                         "dd-MMM-yyyy h:mm", "dd-MMM-yyyy h:mm",
                         "dd-MMM-yyyy hh:mm", "dd-MMM-yyyy hh:mm","dd-MMM-yyyy hh:mm:ss tt",

                         "dd-MMM-yy h:mm:ss tt", "dd-MMM-yy h:mm tt",
                         "dd-MMM-yy hh:mm:ss", "dd-MMM-yy h:mm:ss",
                         "dd-MMM-yy hh:mm tt", "dd-MMM-yy hh tt",
                         "dd-MMM-yy h:mm", "dd-MMM-yy h:mm",
                         "dd-MMM-yy hh:mm", "dd-MMM-yy hh:mm","dd-MMM-yy hh:mm:ss tt",

                         "MM/dd/yy h:mm:ss tt", "MM/dd/yy h:mm tt",
                         "MM/dd/yy hh:mm:ss", "MM/dd/yy h:mm:ss",
                         "MM/dd/yy hh:mm tt", "MM/dd/yy hh tt",
                         "MM/dd/yy h:mm", "MM/dd/yy h:mm",
                         "MM/dd/yy hh:mm", "MM/dd/yy hh:mm","MM/dd/yy hh:mm:ss tt",
                         //,
                         //"dd-MMM-yyyy HH:mm:ss.ffffff","dd-MMM-yyyy hh:mm:ss tt",
                         //"dd-MMM-yyyy H:mm:ss tt", "dd-MMM-yyyy HH:mm tt",
                         //"dd-MMM-yyyy HH:mm:ss", "dd-MMM-yyyy H:mm:ss",
                         //"dd-MMM-yyyy HH:mm tt", "dd-MMM-yyyy HH tt",
                         //"dd-MMM-yyyy H:mm", "dd-MMM-yyyy H:mm",
                         //"dd-MMM-yyyy HH:mm", "dd-MMM-yyyy HH:mm",
                         //"dd-MMM-yyyy HH:mm:ss.ffffff","dd-MMM-yyyy HH:mm:ss tt"
            };

        public bool ValidateEmail(string email)
        {
            System.Text.RegularExpressions.Regex rEmail = new System.Text.RegularExpressions.Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (email.Length > 0)
            {
                if (!rEmail.IsMatch(email))
                {
                    return false;
                }
            }

            return true;
        }

        public DataTable getUserSettings(string emailId, string password)
        {
            Attendance atd = new Attendance();
            DataTable userSettings = JsonConvert.DeserializeObject<DataTable>(atd.GetSettingInfo(emailId, password));
            return userSettings;
        }

        public string AddUser(string fname, string lname, string email)
        {
            Registery reg = new Registery();
            Attendance atd = new Attendance();
            string Message = atd.AddEmployee(fname, lname, email, reg.Read("UserId"), reg.Read("AppId"), reg.Read("InstanceId"));
            return Message;
        }

        public void ReInitializeTimezone()
        {
            Registery reg = new Registery();
            DataTable dt = getUserSettings(reg.Read("EmailId"), reg.Read("Password"));
            reg.Write("Timezone", dt.Rows[0]["timezone"]);
        }

        public void CheckTimezone()
        {
            TimeZoneInfo.ClearCachedData();
            string currentTimeZone = TimeZoneInfo.Local.ToString();

            Loading form = new Loading();
            form.ShowDialog();

            Registery reg = new Registery();
            currentTimeZone = currentTimeZone.Replace(':', '.');
            string cTz = setString(currentTimeZone);

            cTz = cTz.Replace("30", "50");  //API works on .00 and .50

            if (cTz != setString(reg.Read("Timezone")))
            {
                DialogResult dialogue = MessageBox.Show("Your machine Timezone (" + currentTimeZone + ") does not match with Web Application timezone (" + reg.Read("Timezone") + ").\n Would you like to update your Web Application timezone to " + currentTimeZone + " ?", "Time & Attendance", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogue == DialogResult.Yes)
                {
                   

                    foreach (string tz in TimeZone)
                    {
                        if (cTz == setString(tz))
                        {
                            currentTimeZone = tz;
                            break;
                        }
                    }

                    //Attendance atd = new Attendance();
                    //atd.SetTimezone(reg.Read("AppId"), currentTimeZone);
                    SetTimezone(currentTimeZone);

                    MessageBox.Show("Timezone successfully updated to (" + currentTimeZone + ")! ", "Time & Attendance");
                }
                else
                {
                    MessageBox.Show("Your machine Timezone (" + currentTimeZone + ") and Web Application Timezone (" + reg.Read("Timezone") + ") are not same hence you will find Clockin/Clockout time different on both Applications !", "Time & Attendance", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public void SetTimezone(string currentTimeZone)
        {
            Registery reg = new Registery();
            Attendance atd = new Attendance();
            atd.SetTimezone(reg.Read("AppId"), currentTimeZone);
        }

        public string setString(string value)
        {
            return value.Split(new char[] { '(', ')' }, StringSplitOptions.RemoveEmptyEntries)[0];
            
        }

        public void NotifyIcon(object sender)
        {
            Program.m_notifyIcon.ShowBalloonTip(5000, "Time and Attendance", "Your application is running." +
                                        Environment.NewLine, ToolTipIcon.Info);
            Program.m_notifyIcon.Visible = true;

            NotifyIcon_Click((Form)sender);
        }

        public void NotifyIcon_Click(Form form)
        {
            Program.m_notifyIcon.Click += delegate
            {
                form.Show();
                form.Visible = true;
                form.WindowState = FormWindowState.Normal;
            };
        }

        public DateTime FormatDate(string datetime)
        {
            DateTime dt = DateTime.ParseExact(datetime, myDateformats, CultureInfo.InvariantCulture, DateTimeStyles.None);
            return dt;
        }

        public string FirstCharToUpper(string input)
        {
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }

        public void Logout()
        {
            Registery reg = new Registery();
            reg.Write("login", "false");
            reg.Write("UserName", "");
            reg.Write("Password", "");
          
        }

        public void SaveSettings(List<string> weekends, string title)
        {
            Registery reg = new Registery();
            Attendance atd = new Attendance();
            
           // atd.UpdateUserSetting(reg.Read("UserId"),title,weekends);
        }

    }

}
