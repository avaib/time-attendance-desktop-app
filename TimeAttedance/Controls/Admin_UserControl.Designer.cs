﻿namespace TimeAttedance
{
    partial class Admin_UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_name = new System.Windows.Forms.Label();
            this.btn_approve = new System.Windows.Forms.Button();
            this.btn_deny = new System.Windows.Forms.Button();
            this.hdn_rowid = new System.Windows.Forms.TextBox();
            this.hdn_accessTo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(26, 12);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(35, 13);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "label1";
            // 
            // btn_approve
            // 
            this.btn_approve.AutoSize = true;
            this.btn_approve.Location = new System.Drawing.Point(252, 51);
            this.btn_approve.Name = "btn_approve";
            this.btn_approve.Size = new System.Drawing.Size(75, 23);
            this.btn_approve.TabIndex = 1;
            this.btn_approve.Text = "Approve";
            this.btn_approve.UseVisualStyleBackColor = true;
            this.btn_approve.Click += new System.EventHandler(this.btn_approve_Click);
            // 
            // btn_deny
            // 
            this.btn_deny.AutoSize = true;
            this.btn_deny.Location = new System.Drawing.Point(333, 51);
            this.btn_deny.Name = "btn_deny";
            this.btn_deny.Size = new System.Drawing.Size(75, 23);
            this.btn_deny.TabIndex = 2;
            this.btn_deny.Text = "Deny";
            this.btn_deny.UseVisualStyleBackColor = true;
            this.btn_deny.Click += new System.EventHandler(this.btn_deny_Click);
            // 
            // hdn_rowid
            // 
            this.hdn_rowid.Location = new System.Drawing.Point(29, 28);
            this.hdn_rowid.Name = "hdn_rowid";
            this.hdn_rowid.Size = new System.Drawing.Size(100, 20);
            this.hdn_rowid.TabIndex = 3;
            this.hdn_rowid.Visible = false;
            // 
            // hdn_accessTo
            // 
            this.hdn_accessTo.Location = new System.Drawing.Point(29, 53);
            this.hdn_accessTo.Name = "hdn_accessTo";
            this.hdn_accessTo.Size = new System.Drawing.Size(100, 20);
            this.hdn_accessTo.TabIndex = 4;
            this.hdn_accessTo.Visible = false;
            // 
            // Admin_UserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.hdn_accessTo);
            this.Controls.Add(this.hdn_rowid);
            this.Controls.Add(this.btn_deny);
            this.Controls.Add(this.btn_approve);
            this.Controls.Add(this.lbl_name);
            this.Name = "Admin_UserControl";
            this.Size = new System.Drawing.Size(441, 84);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.TextBox hdn_rowid;
        private System.Windows.Forms.TextBox hdn_accessTo;
        public System.Windows.Forms.Button btn_approve;
        public System.Windows.Forms.Button btn_deny;
    }
}
