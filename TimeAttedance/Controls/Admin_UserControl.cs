﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.WebService;
using Newtonsoft.Json;
using System.Collections;

namespace TimeAttedance
{
    public partial class Admin_UserControl : UserControl
    {

        Registery reg = new Registery();
        public Admin_UserControl()
        {
            InitializeComponent();
        }

        public void LoadData()
        {
            Attendance atd = new Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetAdminRightsRequests(Convert.ToInt32(reg.Read("UserId"))));

        }

        public void setValue(DataRow row)
        {           
            lbl_name.Text = row["Title"].ToString() + " requested for \"Admin Rights\" to view\\modify your Clockin/out status";
            hdn_rowid.Text = row["rowid"].ToString();
            hdn_accessTo.Text = row["accessTo"].ToString();
        }

        public void setValue()
        {
            lbl_name.Text = "No Request(s) available!";
            btn_approve.Visible = false;
            btn_deny.Visible = false;
        }

        public void btn_approve_Click(object sender, EventArgs e)
        {
            ApproveDenyRequest("allow", sender);         
        }

        public void btn_deny_Click(object sender, EventArgs e)
        {
            ApproveDenyRequest("deny", sender);          
        }

        public void ApproveDenyRequest(string permission, object sender)
        {
            string rowid1 = (((Control)sender).Parent).Controls["hdn_rowid"].Text;

            Attendance atd = new Attendance();
            int userid = Convert.ToInt32(reg.Read("UserId"));
            int accessTo = Convert.ToInt32((((Control)sender).Parent).Controls["hdn_accessTo"].Text);
            string accessDate = DateTime.Now.ToString();
            string instanceid = reg.Read("InstanceId");
            int rowid = Convert.ToInt32((((Control)sender).Parent).Controls["hdn_rowid"].Text);

            bool result = atd.ApproveDenyAdminRights_Email(userid, accessTo, permission, accessDate, instanceid, rowid);
            //bool result = true;

            if (result)
            {
               // LoadAdminListData();
                MessageBox.Show("Request sent successfully!");
                AdminRights adminrights = new AdminRights();
                adminrights.LoadAdminListData();               
            }
            else
            {
                MessageBox.Show("Request cannot be processed. Please try again!");                
            }
        }
    }
}
