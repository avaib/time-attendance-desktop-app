﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeAttedance.Controls
{
    public partial class SubMenu_UserControl : UserControl
    {
        Main mainObj;
        public SubMenu_UserControl()
        {
            InitializeComponent();
        }

        private void dashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            hideForms();
            ManageTime dashboard = new ManageTime();
            dashboard.Show();
        }
        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            hideForms();
            AddEmployee usersform = new AddEmployee("view");
            usersform.Show();
        }

        private void statisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            hideForms();
            Statistics statsForm = new Statistics();
            statsForm.Show();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            hideForms();
            Settings settingsForm = new Settings();
            settingsForm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainObj = new Main();
            mainObj.Logout();
            hideForms();
            Login loginFrom = new Login();
            loginFrom.Show();
        }

        public void hideForms()
        {
            foreach (Form form in Application.OpenForms)
                form.Hide();
        }


    }
}
