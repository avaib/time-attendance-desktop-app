﻿namespace TimeAttedance.Controls
{
    partial class MainMenu_UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TimeAndAttendance = new System.Windows.Forms.TabPage();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.submenu = new System.Windows.Forms.MenuStrip();
            this.dashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TaskManagement = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.TimeAndAttendance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.submenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TimeAndAttendance);
            this.tabControl1.Controls.Add(this.TaskManagement);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(3, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(832, 561);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            // 
            // TimeAndAttendance
            // 
            this.TimeAndAttendance.AccessibleName = "";
            this.TimeAndAttendance.Controls.Add(this.splitContainer);
            this.TimeAndAttendance.Location = new System.Drawing.Point(4, 34);
            this.TimeAndAttendance.Name = "TimeAndAttendance";
            this.TimeAndAttendance.Padding = new System.Windows.Forms.Padding(3);
            this.TimeAndAttendance.Size = new System.Drawing.Size(824, 523);
            this.TimeAndAttendance.TabIndex = 0;
            this.TimeAndAttendance.Text = "Time And Attendance";
            this.TimeAndAttendance.UseVisualStyleBackColor = true;
            // 
            // splitContainer
            // 
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer.Location = new System.Drawing.Point(6, 3);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.submenu);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.AutoScroll = true;
            this.splitContainer.Panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitContainer.Size = new System.Drawing.Size(812, 514);
            this.splitContainer.SplitterDistance = 105;
            this.splitContainer.TabIndex = 4;
            // 
            // submenu
            // 
            this.submenu.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.submenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.submenu.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.submenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dashboardToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.statisticsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.submenu.Location = new System.Drawing.Point(0, 0);
            this.submenu.Name = "submenu";
            this.submenu.Padding = new System.Windows.Forms.Padding(3, 2, 0, 3);
            this.submenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.submenu.Size = new System.Drawing.Size(101, 512);
            this.submenu.TabIndex = 3;
            this.submenu.Text = "menuStrip1";
            // 
            // dashboardToolStripMenuItem
            // 
            this.dashboardToolStripMenuItem.Name = "dashboardToolStripMenuItem";
            this.dashboardToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.dashboardToolStripMenuItem.Size = new System.Drawing.Size(94, 30);
            this.dashboardToolStripMenuItem.Text = "Dashboard";
            this.dashboardToolStripMenuItem.Click += new System.EventHandler(this.dashboardToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(94, 30);
            this.usersToolStripMenuItem.Text = "Users";
            this.usersToolStripMenuItem.Click += new System.EventHandler(this.usersToolStripMenuItem_Click);
            // 
            // statisticsToolStripMenuItem
            // 
            this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
            this.statisticsToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.statisticsToolStripMenuItem.Size = new System.Drawing.Size(94, 30);
            this.statisticsToolStripMenuItem.Text = "Statistics";
            this.statisticsToolStripMenuItem.Visible = false;
            this.statisticsToolStripMenuItem.Click += new System.EventHandler(this.statisticsToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(94, 30);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Padding = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(94, 30);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // TaskManagement
            // 
            this.TaskManagement.Location = new System.Drawing.Point(4, 34);
            this.TaskManagement.Name = "TaskManagement";
            this.TaskManagement.Padding = new System.Windows.Forms.Padding(3);
            this.TaskManagement.Size = new System.Drawing.Size(824, 523);
            this.TaskManagement.TabIndex = 1;
            this.TaskManagement.Text = "Task Management";
            this.TaskManagement.UseVisualStyleBackColor = true;
            // 
            // MainMenu_UserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Controls.Add(this.tabControl1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "MainMenu_UserControl";
            this.Size = new System.Drawing.Size(846, 574);
            this.tabControl1.ResumeLayout(false);
            this.TimeAndAttendance.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.submenu.ResumeLayout(false);
            this.submenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TimeAndAttendance;
        private System.Windows.Forms.TabPage TaskManagement;
        private System.Windows.Forms.MenuStrip submenu;
        private System.Windows.Forms.ToolStripMenuItem dashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        public System.Windows.Forms.SplitContainer splitContainer;
    }
}
