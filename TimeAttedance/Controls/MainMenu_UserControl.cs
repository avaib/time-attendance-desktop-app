﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeAttedance.Controls
{
    public partial class MainMenu_UserControl : UserControl
    {
        Main mainObj;

        public MainMenu_UserControl()
        {
            InitializeComponent();
            tabControl1.SelectTab(0);
            screenLoad("home");
        }

        public MainMenu_UserControl(String form)
        {
            InitializeComponent();
            tabControl1.SelectTab(0);
            screenLoad(form);
        }
            

        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {
            if (TabIndex == 0)
            {

            }
        }

        private void screenLoad(string form)
        {
            if (form == "adminrights")
            {
                AdminRights adminrightsForm = new AdminRights();
                adminrightsForm.TopLevel = false;
                adminrightsForm.AutoScroll = true;
                splitContainer.Panel2.Controls.Clear();
                splitContainer.Panel2.Controls.Add(adminrightsForm);
                adminrightsForm.FormBorderStyle = FormBorderStyle.None;
                adminrightsForm.Show();
            }
            else if (form == "home")
            {
                ManageTime dashboard = new ManageTime();
                dashboard.TopLevel = false;
                dashboard.AutoScroll = true;
                splitContainer.Panel2.Controls.Clear();
                splitContainer.Panel2.Controls.Add(dashboard);
                dashboard.FormBorderStyle = FormBorderStyle.None;
                dashboard.Show();
            }
            else if (form == "adduser")
            {
                AddEmployee usersForm = new AddEmployee("Add");
                usersForm.TopLevel = false;
                usersForm.AutoScroll = true;
                splitContainer.Panel2.Controls.Clear();
                splitContainer.Panel2.Controls.Add(usersForm);
                usersForm.FormBorderStyle = FormBorderStyle.None;
                usersForm.Show();
            }
            else if (form == "listuser")
            {
                AddEmployee usersForm = new AddEmployee("View");
                usersForm.TopLevel = false;
                usersForm.AutoScroll = true;
                splitContainer.Panel2.Controls.Clear();
                splitContainer.Panel2.Controls.Add(usersForm);
                usersForm.FormBorderStyle = FormBorderStyle.None;
                usersForm.Show();
            }
        }
        
        private void dashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageTime dashboard = new ManageTime();
            dashboard.TopLevel = false;
            dashboard.AutoScroll = true;
            splitContainer.Panel2.Controls.Clear();
            splitContainer.Panel2.Controls.Add(dashboard);
            dashboard.FormBorderStyle = FormBorderStyle.None;
            dashboard.Show();

        }
        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddEmployee usersForm = new AddEmployee("view");
            usersForm.TopLevel = false;
            usersForm.AutoScroll = true;
            splitContainer.Panel2.Controls.Clear();
            splitContainer.Panel2.Controls.Add(usersForm);
            usersForm.FormBorderStyle = FormBorderStyle.None;
            usersForm.Show();
        }

        private void statisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Statistics statsForm = new Statistics();
            statsForm.TopLevel = false;
            statsForm.AutoScroll = true;
            splitContainer.Panel2.Controls.Clear();
            splitContainer.Panel2.Controls.Add(statsForm);
            statsForm.FormBorderStyle = FormBorderStyle.None;
            statsForm.Show();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settingsForm = new Settings();
            settingsForm.TopLevel = false;
            settingsForm.AutoScroll = true;
            splitContainer.Panel2.Controls.Clear();
            splitContainer.Panel2.Controls.Add(settingsForm);
            settingsForm.FormBorderStyle = FormBorderStyle.None;
            settingsForm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mainObj = new Main();
            mainObj.Logout();

            foreach (Form form in Application.OpenForms)
                form.Hide();

            Login loginFrom = new Login();
            loginFrom.Show();
        }



    }
}
