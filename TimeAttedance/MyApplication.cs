﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.Controls;

namespace TimeAttedance
{
    public partial class MyApplication : Form
    {
        Main obj = new Main();

        public MyApplication()
        {
            InitializeComponent();
            onLoad();
            MainMenu_UserControl mainMenu = new MainMenu_UserControl();
            main_panel.Controls.Add(mainMenu);
        }

        public MyApplication(String form)
        {
            InitializeComponent();
            MainMenu_UserControl mainMenu = new MainMenu_UserControl(form);
            main_panel.Controls.Add(mainMenu);
        }

        private void onLoad()
        {
            obj.ReInitializeTimezone();
            obj.CheckTimezone();

        }

        //private void MyApplication_Load(object sender, EventArgs e)
        //{
        //    MainMenu_UserControl mainMenu = new MainMenu_UserControl();
        //    main_panel.Controls.Add(mainMenu);
        //}
    }
}
