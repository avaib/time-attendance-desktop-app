﻿namespace TimeAttedance
{
    partial class AddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEmployee));
            this.lblTitle = new System.Windows.Forms.Label();
            this.addemp_panel = new System.Windows.Forms.Panel();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtLname = new System.Windows.Forms.TextBox();
            this.txtFname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.viewemp_panel = new System.Windows.Forms.Panel();
            this.grd_employees = new System.Windows.Forms.DataGridView();
            this.subMenu = new System.Windows.Forms.MenuStrip();
            this.inactiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revokeAccessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.btnLoadUser = new System.Windows.Forms.Button();
            this.addemp_panel.SuspendLayout();
            this.viewemp_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_employees)).BeginInit();
            this.subMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(33, 35);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(86, 31);
            this.lblTitle.TabIndex = 9;
            this.lblTitle.Text = "Users";
            // 
            // addemp_panel
            // 
            this.addemp_panel.Controls.Add(this.btnAddEmployee);
            this.addemp_panel.Controls.Add(this.label3);
            this.addemp_panel.Controls.Add(this.txtEmail);
            this.addemp_panel.Controls.Add(this.txtLname);
            this.addemp_panel.Controls.Add(this.txtFname);
            this.addemp_panel.Controls.Add(this.label2);
            this.addemp_panel.Controls.Add(this.label1);
            this.addemp_panel.Location = new System.Drawing.Point(12, 69);
            this.addemp_panel.Name = "addemp_panel";
            this.addemp_panel.Size = new System.Drawing.Size(544, 305);
            this.addemp_panel.TabIndex = 11;
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Location = new System.Drawing.Point(222, 144);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(75, 23);
            this.btnAddEmployee.TabIndex = 17;
            this.btnAddEmployee.Text = "Add";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(102, 97);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(195, 20);
            this.txtEmail.TabIndex = 15;
            // 
            // txtLname
            // 
            this.txtLname.Location = new System.Drawing.Point(102, 60);
            this.txtLname.Name = "txtLname";
            this.txtLname.Size = new System.Drawing.Size(195, 20);
            this.txtLname.TabIndex = 14;
            // 
            // txtFname
            // 
            this.txtFname.Location = new System.Drawing.Point(102, 27);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(195, 20);
            this.txtFname.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Last Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "First Name";
            // 
            // viewemp_panel
            // 
            this.viewemp_panel.Controls.Add(this.grd_employees);
            this.viewemp_panel.Controls.Add(this.subMenu);
            this.viewemp_panel.Location = new System.Drawing.Point(12, 72);
            this.viewemp_panel.Name = "viewemp_panel";
            this.viewemp_panel.Size = new System.Drawing.Size(390, 299);
            this.viewemp_panel.TabIndex = 19;
            // 
            // grd_employees
            // 
            this.grd_employees.AllowUserToAddRows = false;
            this.grd_employees.AllowUserToDeleteRows = false;
            this.grd_employees.AllowUserToResizeColumns = false;
            this.grd_employees.AllowUserToResizeRows = false;
            this.grd_employees.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grd_employees.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_employees.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grd_employees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_employees.Cursor = System.Windows.Forms.Cursors.Default;
            this.grd_employees.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.grd_employees.Location = new System.Drawing.Point(3, 30);
            this.grd_employees.Name = "grd_employees";
            this.grd_employees.ReadOnly = true;
            this.grd_employees.RowHeadersVisible = false;
            this.grd_employees.RowHeadersWidth = 30;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Gray;
            this.grd_employees.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grd_employees.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grd_employees.Size = new System.Drawing.Size(538, 266);
            this.grd_employees.TabIndex = 19;
            this.grd_employees.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_employees_CellContentClick);
            // 
            // subMenu
            // 
            this.subMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inactiveToolStripMenuItem,
            this.revokeAccessToolStripMenuItem});
            this.subMenu.Location = new System.Drawing.Point(0, 0);
            this.subMenu.Name = "subMenu";
            this.subMenu.Size = new System.Drawing.Size(390, 24);
            this.subMenu.TabIndex = 20;
            this.subMenu.Text = "menuStrip2";
            // 
            // inactiveToolStripMenuItem
            // 
            this.inactiveToolStripMenuItem.Name = "inactiveToolStripMenuItem";
            this.inactiveToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.inactiveToolStripMenuItem.Text = "Inactive Users";
            this.inactiveToolStripMenuItem.Click += new System.EventHandler(this.inactiveToolStripMenuItem_Click);
            // 
            // revokeAccessToolStripMenuItem
            // 
            this.revokeAccessToolStripMenuItem.Name = "revokeAccessToolStripMenuItem";
            this.revokeAccessToolStripMenuItem.Size = new System.Drawing.Size(149, 20);
            this.revokeAccessToolStripMenuItem.Text = "Admin Rights Request(s)";
            this.revokeAccessToolStripMenuItem.Click += new System.EventHandler(this.adminRightaccessToolStripMenuItem_Click);
            // 
            // btnAddUser
            // 
            this.btnAddUser.Location = new System.Drawing.Point(478, 42);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(75, 23);
            this.btnAddUser.TabIndex = 20;
            this.btnAddUser.Text = "Add User(s)";
            this.btnAddUser.UseVisualStyleBackColor = true;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // btnLoadUser
            // 
            this.btnLoadUser.Location = new System.Drawing.Point(478, 43);
            this.btnLoadUser.Name = "btnLoadUser";
            this.btnLoadUser.Size = new System.Drawing.Size(75, 23);
            this.btnLoadUser.TabIndex = 21;
            this.btnLoadUser.Text = "View User(s)";
            this.btnLoadUser.UseVisualStyleBackColor = true;
            this.btnLoadUser.Visible = false;
            this.btnLoadUser.Click += new System.EventHandler(this.btnLoadUser_Click);
            // 
            // AddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 471);
            this.Controls.Add(this.btnLoadUser);
            this.Controls.Add(this.btnAddUser);
            this.Controls.Add(this.viewemp_panel);
            this.Controls.Add(this.addemp_panel);
            this.Controls.Add(this.lblTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddEmployee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My Network";
            this.addemp_panel.ResumeLayout(false);
            this.addemp_panel.PerformLayout();
            this.viewemp_panel.ResumeLayout(false);
            this.viewemp_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_employees)).EndInit();
            this.subMenu.ResumeLayout(false);
            this.subMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel addemp_panel;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtLname;
        private System.Windows.Forms.TextBox txtFname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel viewemp_panel;
        private System.Windows.Forms.DataGridView grd_employees;
        private System.Windows.Forms.MenuStrip subMenu;
        private System.Windows.Forms.ToolStripMenuItem inactiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revokeAccessToolStripMenuItem;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.Button btnLoadUser;
    }
}