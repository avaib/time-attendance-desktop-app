﻿namespace TimeAttedance
{
    partial class AdminRights
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminRights));
            this.adminrequest_panel = new System.Windows.Forms.Panel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.grd_accessRightsRequest = new System.Windows.Forms.DataGridView();
            this.employeeHistory_panel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.backToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminrequest_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grd_accessRightsRequest)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // adminrequest_panel
            // 
            this.adminrequest_panel.Controls.Add(this.progressBar);
            this.adminrequest_panel.Controls.Add(this.grd_accessRightsRequest);
            this.adminrequest_panel.Location = new System.Drawing.Point(12, 23);
            this.adminrequest_panel.Name = "adminrequest_panel";
            this.adminrequest_panel.Size = new System.Drawing.Size(545, 357);
            this.adminrequest_panel.TabIndex = 2;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 327);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(507, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 3;
            this.progressBar.Value = 5;
            this.progressBar.Visible = false;
            // 
            // grd_accessRightsRequest
            // 
            this.grd_accessRightsRequest.AllowUserToAddRows = false;
            this.grd_accessRightsRequest.AllowUserToDeleteRows = false;
            this.grd_accessRightsRequest.AllowUserToResizeColumns = false;
            this.grd_accessRightsRequest.AllowUserToResizeRows = false;
            this.grd_accessRightsRequest.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.grd_accessRightsRequest.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grd_accessRightsRequest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grd_accessRightsRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grd_accessRightsRequest.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.grd_accessRightsRequest.Location = new System.Drawing.Point(15, 232);
            this.grd_accessRightsRequest.Name = "grd_accessRightsRequest";
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Gray;
            this.grd_accessRightsRequest.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grd_accessRightsRequest.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.grd_accessRightsRequest.Size = new System.Drawing.Size(418, 286);
            this.grd_accessRightsRequest.TabIndex = 2;
            this.grd_accessRightsRequest.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grd_accessRightsRequest_CellContentClick);
            // 
            // employeeHistory_panel
            // 
            this.employeeHistory_panel.Location = new System.Drawing.Point(-3, 27);
            this.employeeHistory_panel.Name = "employeeHistory_panel";
            this.employeeHistory_panel.Size = new System.Drawing.Size(528, 343);
            this.employeeHistory_panel.TabIndex = 4;
            this.employeeHistory_panel.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(559, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // backToolStripMenuItem
            // 
            this.backToolStripMenuItem.Name = "backToolStripMenuItem";
            this.backToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.backToolStripMenuItem.Text = "Back";
            this.backToolStripMenuItem.Click += new System.EventHandler(this.backToolStripMenuItem_Click);
            // 
            // AdminRights
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(559, 381);
            this.Controls.Add(this.employeeHistory_panel);
            this.Controls.Add(this.adminrequest_panel);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AdminRights";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminRights";
            this.adminrequest_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grd_accessRightsRequest)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel adminrequest_panel;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.DataGridView grd_accessRightsRequest;
        private System.Windows.Forms.Panel employeeHistory_panel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem backToolStripMenuItem;
    }
}