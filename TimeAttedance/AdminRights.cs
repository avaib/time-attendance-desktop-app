﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.WebService;

namespace TimeAttedance
{
    public partial class AdminRights : Form
    {
        Main obj = new Main();
        Registery reg;

        public AdminRights()
        {
            InitializeComponent();
            reg = new Registery();
            employeeHistory_panel.Visible = false;
            LoadAdminListData();
            adminrequest_panel.Visible = true;
        }

        public AdminRights(string rowid, string empid)
        {
            InitializeComponent();
            reg = new Registery();
            adminrequest_panel.Visible = false;
            LoadEmpData();
            employeeHistory_panel.Visible = true;
        }

        //View 'Clockin/out stats of specific Employee'
        public void LoadEmpData()
        {

        }

        public void LoadAdminListData()
        {
            Attendance atd = new Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetAdminRightsRequests(Convert.ToInt32(reg.Read("UserId"))));

            //grd_accessRightsRequest.DataSource = null;
            //grd_accessRightsRequest.Rows.Clear();
            //grd_accessRightsRequest.AutoGenerateColumns = false;
            //grd_accessRightsRequest.ColumnCount = 5;

            //grd_accessRightsRequest.Columns[0].Name = "rowid";
            //grd_accessRightsRequest.Columns[0].DataPropertyName = "rowid";
            //grd_accessRightsRequest.Columns[0].Visible = false;

            //grd_accessRightsRequest.Columns[1].Name = "accessTo";
            //grd_accessRightsRequest.Columns[1].DataPropertyName = "accessTo";
            //grd_accessRightsRequest.Columns[1].Visible = false;

            //grd_accessRightsRequest.Columns[2].Name = "title";
            //grd_accessRightsRequest.Columns[2].HeaderText = "Title";
            //grd_accessRightsRequest.Columns[2].DataPropertyName = "Title";

            //grd_accessRightsRequest.Columns[5].Name = "action";
            //grd_accessRightsRequest.Columns[5].HeaderText = "";

            var allowColumn = new DataGridViewButtonColumn
            {
                Text = "Allow",
                UseColumnTextForButtonValue = true,
                Name = "allow",
                DataPropertyName = "rowid",
            };
            grd_accessRightsRequest.Columns.Add(allowColumn);

            var denyColumn = new DataGridViewButtonColumn
            {
                Text = "Deny",
                UseColumnTextForButtonValue = true,
                Name = "deny",
                DataPropertyName = "rowid",
            };
            grd_accessRightsRequest.Columns.Add(denyColumn);
            grd_accessRightsRequest.Visible = false;
            if (dt.Rows.Count > 0)
            {
                // grd_accessRightsRequest.DataSource = dt;
                //grd_accessRightsRequest.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                // grd_accessRightsRequest.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                //foreach (DataGridViewRow row in grd_accessRightsRequest.Rows)
                //{
                //    DataGridViewComboBoxCell ComboBoxCell = new DataGridViewComboBoxCell();
                //    ComboBoxCell.Items.AddRange(new string[] { "Approve", "Deny" });
                //    row.Cells["action"] = ComboBoxCell;
                //    row.Cells["action"].Value = "Approve";
                //}
                int i = 1;
                string name = "";
                foreach (DataRow row in dt.Rows)
                {
                    int x = 0;
                    name = "control" + i;
                    if(i == 1)
                    x= i + 8;
                    Admin_UserControl control = new Admin_UserControl();                
                    control.Location = new System.Drawing.Point(10 + (5), 0 + (x * 10));
                    control.setValue(row);
                    adminrequest_panel.Controls.Add(control);

                    i++;
                }
            }
            else
            {
                //grd_accessRightsRequest.Rows.Add(1);
                //grd_accessRightsRequest[0, 0].Value = "";
                //grd_accessRightsRequest[1, 0].Value = "No Request(s) available!";

                Admin_UserControl control = new Admin_UserControl();
                control.Location = new System.Drawing.Point(10 + (5), 30);
                control.setValue();                
            }
        }

        private void grd_accessRightsRequest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                progressBar.Visible = true;
                Attendance atd = new Attendance();
                int userid = Convert.ToInt32(reg.Read("UserId"));
                int accessTo = Convert.ToInt32(grd_accessRightsRequest.Rows[e.RowIndex].Cells["accessTo"].Value);
                string permission = (senderGrid.Columns[e.ColumnIndex].HeaderText == "allow") ? "allow" : "deny";
                string accessDate = DateTime.Now.ToString();
                string instanceid = reg.Read("InstanceId");
                int rowid = Convert.ToInt32(grd_accessRightsRequest.Rows[e.RowIndex].Cells["rowid"].Value);

                bool result = atd.ApproveDenyAdminRights_Email(userid, accessTo, permission, accessDate, instanceid, rowid);
                progressBar.Visible = false;
                if (result)
                {
                    senderGrid.Columns[e.ColumnIndex].ReadOnly = true;
                    grd_accessRightsRequest.Update();

                    LoadAdminListData();
                    MessageBox.Show("Request sent successfully!");
                }
                else
                {
                    MessageBox.Show("Request cannot be processed. Please try again!");
                }
            }
        }

        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ParentForm.Hide();
            MyApplication HomePage = new MyApplication("listuser");
            HomePage.Show();
        }
    }
}
