﻿namespace TimeAttedance
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTimezone = new System.Windows.Forms.Label();
            this.lblWeekend = new System.Windows.Forms.Label();
            this.txtfullname = new System.Windows.Forms.TextBox();
            this.comboBox_timezone = new System.Windows.Forms.ComboBox();
            this.addHoliday_panel = new System.Windows.Forms.Panel();
            this.btn_AddHoliday = new System.Windows.Forms.Button();
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.chk_yearly = new System.Windows.Forms.CheckBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblname = new System.Windows.Forms.Label();
            this.listHoliday_panel = new System.Windows.Forms.Panel();
            this.lnkHide = new System.Windows.Forms.LinkLabel();
            this.Holidays_grdview = new System.Windows.Forms.DataGridView();
            this.lblHolidays = new System.Windows.Forms.Label();
            this.listWeekends = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lnkHolidays = new System.Windows.Forms.LinkLabel();
            this.addHoliday_panel.SuspendLayout();
            this.listHoliday_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Holidays_grdview)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(32, 20);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(54, 13);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Full Name";
            // 
            // lblTimezone
            // 
            this.lblTimezone.AutoSize = true;
            this.lblTimezone.Location = new System.Drawing.Point(32, 53);
            this.lblTimezone.Name = "lblTimezone";
            this.lblTimezone.Size = new System.Drawing.Size(53, 13);
            this.lblTimezone.TabIndex = 1;
            this.lblTimezone.Text = "Timezone";
            // 
            // lblWeekend
            // 
            this.lblWeekend.AutoSize = true;
            this.lblWeekend.Location = new System.Drawing.Point(32, 83);
            this.lblWeekend.Name = "lblWeekend";
            this.lblWeekend.Size = new System.Drawing.Size(54, 13);
            this.lblWeekend.TabIndex = 2;
            this.lblWeekend.Text = "Weekend";
            // 
            // txtfullname
            // 
            this.txtfullname.Location = new System.Drawing.Point(90, 20);
            this.txtfullname.Name = "txtfullname";
            this.txtfullname.Size = new System.Drawing.Size(200, 20);
            this.txtfullname.TabIndex = 4;
            // 
            // comboBox_timezone
            // 
            this.comboBox_timezone.FormattingEnabled = true;
            this.comboBox_timezone.Location = new System.Drawing.Point(90, 45);
            this.comboBox_timezone.Name = "comboBox_timezone";
            this.comboBox_timezone.Size = new System.Drawing.Size(370, 21);
            this.comboBox_timezone.TabIndex = 5;
            // 
            // addHoliday_panel
            // 
            this.addHoliday_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.addHoliday_panel.Controls.Add(this.btn_AddHoliday);
            this.addHoliday_panel.Controls.Add(this.monthCalendar);
            this.addHoliday_panel.Controls.Add(this.txtTitle);
            this.addHoliday_panel.Controls.Add(this.chk_yearly);
            this.addHoliday_panel.Controls.Add(this.lblDate);
            this.addHoliday_panel.Controls.Add(this.lblname);
            this.addHoliday_panel.Location = new System.Drawing.Point(92, 183);
            this.addHoliday_panel.Name = "addHoliday_panel";
            this.addHoliday_panel.Size = new System.Drawing.Size(332, 245);
            this.addHoliday_panel.TabIndex = 12;
            // 
            // btn_AddHoliday
            // 
            this.btn_AddHoliday.Location = new System.Drawing.Point(242, 206);
            this.btn_AddHoliday.Name = "btn_AddHoliday";
            this.btn_AddHoliday.Size = new System.Drawing.Size(75, 23);
            this.btn_AddHoliday.TabIndex = 20;
            this.btn_AddHoliday.Text = "Add";
            this.btn_AddHoliday.UseVisualStyleBackColor = true;
            this.btn_AddHoliday.Click += new System.EventHandler(this.btn_AddHoliday_Click);
            // 
            // monthCalendar
            // 
            this.monthCalendar.Location = new System.Drawing.Point(76, 31);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 19;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(76, 7);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(241, 20);
            this.txtTitle.TabIndex = 18;
            // 
            // chk_yearly
            // 
            this.chk_yearly.AutoSize = true;
            this.chk_yearly.Location = new System.Drawing.Point(76, 196);
            this.chk_yearly.Name = "chk_yearly";
            this.chk_yearly.Size = new System.Drawing.Size(76, 17);
            this.chk_yearly.TabIndex = 17;
            this.chk_yearly.Text = "Every year";
            this.chk_yearly.UseVisualStyleBackColor = true;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(20, 31);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 16;
            this.lblDate.Text = "Date";
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Location = new System.Drawing.Point(20, 10);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(27, 13);
            this.lblname.TabIndex = 15;
            this.lblname.Text = "Title";
            // 
            // listHoliday_panel
            // 
            this.listHoliday_panel.Controls.Add(this.lnkHide);
            this.listHoliday_panel.Controls.Add(this.Holidays_grdview);
            this.listHoliday_panel.Location = new System.Drawing.Point(84, 183);
            this.listHoliday_panel.Name = "listHoliday_panel";
            this.listHoliday_panel.Size = new System.Drawing.Size(354, 245);
            this.listHoliday_panel.TabIndex = 21;
            this.listHoliday_panel.Visible = false;
            // 
            // lnkHide
            // 
            this.lnkHide.AutoSize = true;
            this.lnkHide.Location = new System.Drawing.Point(319, 11);
            this.lnkHide.Name = "lnkHide";
            this.lnkHide.Size = new System.Drawing.Size(28, 17);
            this.lnkHide.TabIndex = 2;
            this.lnkHide.TabStop = true;
            this.lnkHide.Text = "Hide";
            this.lnkHide.UseCompatibleTextRendering = true;
            this.lnkHide.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lnkHide.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkHide_LinkClicked);
            // 
            // Holidays_grdview
            // 
            this.Holidays_grdview.AllowUserToAddRows = false;
            this.Holidays_grdview.AllowUserToDeleteRows = false;
            this.Holidays_grdview.AllowUserToResizeColumns = false;
            this.Holidays_grdview.AllowUserToResizeRows = false;
            this.Holidays_grdview.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Holidays_grdview.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Holidays_grdview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Holidays_grdview.DefaultCellStyle = dataGridViewCellStyle1;
            this.Holidays_grdview.Location = new System.Drawing.Point(22, 8);
            this.Holidays_grdview.Name = "Holidays_grdview";
            this.Holidays_grdview.RowHeadersVisible = false;
            this.Holidays_grdview.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Holidays_grdview.Size = new System.Drawing.Size(290, 234);
            this.Holidays_grdview.TabIndex = 1;
            this.Holidays_grdview.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Holidays_grdview_CellClick);
            // 
            // lblHolidays
            // 
            this.lblHolidays.AutoSize = true;
            this.lblHolidays.Location = new System.Drawing.Point(32, 190);
            this.lblHolidays.Name = "lblHolidays";
            this.lblHolidays.Size = new System.Drawing.Size(47, 13);
            this.lblHolidays.TabIndex = 14;
            this.lblHolidays.Text = "Holidays";
            // 
            // listWeekends
            // 
            this.listWeekends.FormattingEnabled = true;
            this.listWeekends.Location = new System.Drawing.Point(92, 82);
            this.listWeekends.Name = "listWeekends";
            this.listWeekends.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listWeekends.Size = new System.Drawing.Size(120, 95);
            this.listWeekends.TabIndex = 15;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(444, 403);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 16;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lnkHolidays
            // 
            this.lnkHolidays.AutoSize = true;
            this.lnkHolidays.Location = new System.Drawing.Point(431, 183);
            this.lnkHolidays.Name = "lnkHolidays";
            this.lnkHolidays.Size = new System.Drawing.Size(73, 13);
            this.lnkHolidays.TabIndex = 17;
            this.lnkHolidays.TabStop = true;
            this.lnkHolidays.Text = "View Holidays";
            this.lnkHolidays.VisitedLinkColor = System.Drawing.Color.Blue;
            this.lnkHolidays.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkHolidays_LinkClicked);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 471);
            this.Controls.Add(this.listHoliday_panel);
            this.Controls.Add(this.lnkHolidays);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.listWeekends);
            this.Controls.Add(this.addHoliday_panel);
            this.Controls.Add(this.comboBox_timezone);
            this.Controls.Add(this.txtfullname);
            this.Controls.Add(this.lblWeekend);
            this.Controls.Add(this.lblHolidays);
            this.Controls.Add(this.lblTimezone);
            this.Controls.Add(this.lblTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.addHoliday_panel.ResumeLayout(false);
            this.addHoliday_panel.PerformLayout();
            this.listHoliday_panel.ResumeLayout(false);
            this.listHoliday_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Holidays_grdview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblTimezone;
        private System.Windows.Forms.Label lblWeekend;
        private System.Windows.Forms.TextBox txtfullname;
        private System.Windows.Forms.ComboBox comboBox_timezone;
        private System.Windows.Forms.Panel addHoliday_panel;
        private System.Windows.Forms.Button btn_AddHoliday;
        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.CheckBox chk_yearly;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label lblHolidays;
        private System.Windows.Forms.ListBox listWeekends;
        private System.Windows.Forms.Panel listHoliday_panel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.LinkLabel lnkHolidays;
        private System.Windows.Forms.DataGridView Holidays_grdview;
        private System.Windows.Forms.LinkLabel lnkHide;
    }
}