﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using TimeAttedance.WebService;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using Newtonsoft;
using Newtonsoft.Json;
//using System.Reflection;
//using IWshRuntimeLibrary;
//using Microsoft.CSharp;

namespace TimeAttedance
{
    public partial class Login : Form
    {
        Registery reg;
        public string instanceId = "";
        public string login = "true";
        Main obj = new Main();

        public Login()
        {
            InitializeComponent();
            //reg = new Registery();
            //reg.IsConnected();

            txtEmail.Focus();
            //ManageTime mt = new ManageTime("");
            //mt.Close();       
        }


        public DataTable JsonStringToDataTable(string jsonString)
        {
            DataTable dt = new DataTable();
            string[] jsonStringArray = Regex.Split(jsonString.Replace("[", "").Replace("]", ""), "},{");
            List<string> ColumnsName = new List<string>();
            foreach (string jSA in jsonStringArray)
            {
                string[] jsonStringData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                foreach (string ColumnsNameData in jsonStringData)
                {
                    try
                    {
                        int idx = ColumnsNameData.IndexOf(":");
                        string ColumnsNameString = ColumnsNameData.Substring(0, idx - 1).Replace("\"", "");
                        if (!ColumnsName.Contains(ColumnsNameString))
                        {
                            ColumnsName.Add(ColumnsNameString);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Error Parsing Column Name : {0}", ColumnsNameData));
                    }
                }
                break;
            }
            foreach (string AddColumnName in ColumnsName)
            {
                dt.Columns.Add(AddColumnName);
            }
            foreach (string jSA in jsonStringArray)
            {
                string[] RowData = Regex.Split(jSA.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in RowData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string RowColumns = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string RowDataString = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[RowColumns] = RowDataString;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                dt.Rows.Add(nr);
            }
            return dt;
        }
        private void btnLoginIn_Click(object sender, EventArgs e)
        {
            if (obj.ValidateEmail(txtEmail.Text))
            {
                if (txtEmail.Text == "")
                {
                    MessageBox.Show("Enter Email Id", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                    return;
                }
                if (txtPassword.Text == "")
                {
                    MessageBox.Show("Enter Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPassword.Focus();
                    return;
                }
                reg = new Registery();
                Attendance atd = new Attendance();
                string emailId = txtEmail.Text;
                string password = txtPassword.Text;
                string jsonText = atd.VerifyLogin(emailId, password);

                if (jsonText != null)
                {
                    if (jsonText == "TRUE")
                    {
                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetLoginInfo(txtEmail.Text));

                        if (dt.Rows.Count > 0)
                        {
                            instanceId = dt.Rows[0]["instanceId"].ToString();
                            //if (reg.Read("InstanceId") == null)
                            //{
                            reg.Write("InstanceId", instanceId);
                            //}
                            //else
                            //{
                            //    instanceId = reg.Read("InstanceId");
                            //}

                            reg.Write("login", "true");
                            reg.Write("EmailId", txtEmail.Text);
                            reg.Write("Password", txtPassword.Text);
                            reg.Write("UserId", dt.Rows[0]["ID"].ToString());
                            reg.Write("Title", dt.Rows[0]["Title"].ToString());

                            //User Instance Settings
                            DataTable userSettings = obj.getUserSettings(emailId, password);

                            //Settings
                            reg.Write("Timezone", userSettings.Rows[0]["timezone"]);
                            reg.Write("AppId", userSettings.Rows[0]["userid"]);

                            //RememberMe Checkbox
                            if (RememberMe.Checked)
                            {
                                //Properties.Settings.Default.UserName = txtEmail.Text;
                                //Properties.Settings.Default.Password = txtPassword.Text;
                                //Properties.Settings.Default.Save();

                                //Save Credentials in Registry
                                reg.Write("UserName", txtEmail.Text);
                                //reg.Write("Password", txtPassword.Text);
                            }

                            this.Hide();
                            MyApplication app = new MyApplication();
                            app.Show();
                            //ManageTime mt = new ManageTime();
                            //mt.Show();
                        }
                        else
                        {

                            MessageBox.Show("Email Id doesnot exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtEmail.Focus();
                        }

                    }
                    else
                    {
                        MessageBox.Show("Invalid Email Id/Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtEmail.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Email Id/Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                }
            }
            else
            {
                MessageBox.Show("Please provide valid Email Address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmail.SelectAll();
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignUp signup = new SignUp();
            signup.Show();
        }

    }
}
