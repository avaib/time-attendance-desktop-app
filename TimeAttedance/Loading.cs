﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeAttedance
{
    public partial class Loading : Form
    {
        public Loading()
        {
            InitializeComponent();
        }

        public Loading(String formname)
        {
            InitializeComponent();
            loader.Visible = true;
            if (formname == "manage")
            {
                ManageTime mg = new ManageTime();
                mg.Show();
            }
            else if (formname == "login")
            {
                Login lg = new Login();
                lg.Show();
            }
            else if (formname == "employee" || formname == "addemployee")
            {
                AddEmployee emp = new AddEmployee();
                emp.Show();
            }
            else if (formname == "error")
            {
                this.Close();
            }
        }

        private void Loading_Load(object sender, EventArgs e)
        {
            loader.Visible = true;
            timer.Interval = 3000;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            loader.Visible = false;
            this.Hide();
        }

        
    }
}
