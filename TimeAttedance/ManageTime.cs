﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TimeAttedance.WebService;
using TimeAttedance.Controls;
using System.Linq;
//using NotificationWindow;

namespace TimeAttedance
{
    public partial class ManageTime : Form
    {
        public string instanceId = "";
        Registery reg = new Registery();
        string currentTimeZone = TimeZoneInfo.Local.ToString();
        Main obj = new Main();
        public static int usersRowsCount = 0;
        public static DataTable usersDt = null;
        //PopupNotifier popupNotifier1;

        public ManageTime()
        {
            //CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            //obj.ReInitializeTimezone();
            //obj.CheckTimezone();


            //popupNotifier1 = new NotificationWindow.PopupNotifier();
            //popupNotifier1.AnimationDuration = 250;
            //popupNotifier1.AnimationInterval = 1;
            //popupNotifier1.BodyColor = System.Drawing.SystemColors.GradientActiveCaption;
            //popupNotifier1.BorderColor = System.Drawing.Color.Aqua;
            //popupNotifier1.ButtonBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            //popupNotifier1.ContentFont = new System.Drawing.Font("Tahoma", 8F);
            //popupNotifier1.ContentPadding = new System.Windows.Forms.Padding(0, 17, 0, 0);
            //popupNotifier1.ContentText = null;
            //popupNotifier1.GradientPower = 300;
            //popupNotifier1.HeaderColor = System.Drawing.Color.SteelBlue;
            //popupNotifier1.HeaderFont = new System.Drawing.Font("Bookman Old Style", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //popupNotifier1.HeaderHeight = 20;
            //popupNotifier1.HeaderPadding = new System.Windows.Forms.Padding(0);
            //popupNotifier1.HeaderText = "Header Text";
            ////  popupNotifier1.Image = global::try_pop_up.Properties.Resources.DispatcherIcon;
            //popupNotifier1.ImagePadding = new System.Windows.Forms.Padding(10, 13, 0, 0);
            //popupNotifier1.ImageSize = new System.Drawing.Size(30, 30);
            //popupNotifier1.OptionsMenu = null;
            //popupNotifier1.Scroll = false;
            //popupNotifier1.ShowCloseButton = false;
            //popupNotifier1.Size = new System.Drawing.Size(220, 75);
            //popupNotifier1.TitleColor = System.Drawing.Color.Black;
            //popupNotifier1.TitleFont = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //popupNotifier1.TitlePadding = new System.Windows.Forms.Padding(0, 2, 0, 0);
            //popupNotifier1.TitleText = "Hello";
            //popupNotifier1.TitleText = "Hello";
            //popupNotifier1.ContentText = "content text";
            //popupNotifier1.ShowCloseButton = true;
            //popupNotifier1.ShowOptionsButton = false;
            //popupNotifier1.ShowGripText = true;
            //popupNotifier1.Delay = 5000;
            //popupNotifier1.AnimationInterval = 1;
            //popupNotifier1.AnimationDuration = 400;
            //popupNotifier1.Scroll = true;
            //popupNotifier1.ShowCloseButton = true;
            //popupNotifier1.Image = Properties.Resources._2;
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reg.Write("login", "false");

            reg.Write("UserName", "");
            reg.Write("Password", "");

            this.Hide();
            Login form = new Login();
            form.Show();
        }

        private void signup()
        {
            txtEmail.Visible = true;
            btnOk.Visible = true;
            btnClockin.Visible = false;
        }

        private void btnClockin_Click(object sender, EventArgs e)
        {

            if (reg.Read("login") == "false")
            {
                signup();
            }
            else
            {
                progressBar1.Visible = true;
                btnClockin.Enabled = false;
                TempUser user = new TempUser
                {
                    userid = reg.Read("UserId"),
                    instanceid = reg.Read("InstanceId"),
                    rowid = reg.Read("RowId")
                };


                BW_Clock.RunWorkerAsync(user);
            }
        }
        private void SetStatus(bool clockin)
        {
            progressBar1.Visible = true;
            btnClockin.Enabled = false;
            //new
            btnClockout.Enabled = false;

            TempUser user = new TempUser
            {
                userid = reg.Read("UserId"),
                clockin = clockin
            };

            BW_Setstatus.RunWorkerAsync(user);

        }

        private void ManageTime_Load(object sender, EventArgs e)
        {
            if (reg.Read("login") == null)
            {
                reg.Write("login", "false");
            }
            //if (reg.Read("login") == "false")
            //{

            //}
            else
            {
                SetStatus(false);
            }

            LoadUsers();

        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            btnClockin.Enabled = false;
            TempUser user = new TempUser
            {
                email = txtEmail.Text,
                password = "ajsdh!@#kasd"
            };


            BW_Isregistered.RunWorkerAsync(user);

        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            btnClockin.Enabled = false;
            TempUser user = new TempUser
            {
                email = txtEmail.Text,
                password = txtPassword.Text
            };

            BW_Login.RunWorkerAsync(user);


        }
        private void BW_Signup_DoWork(object sender, DoWorkEventArgs e)
        {
            TempUser user = e.Argument as TempUser;
            result r = new result(2);
            AddUser.AddUser adduser = new AddUser.AddUser();



            string result = adduser.signup(user.email, user.password, user.parent_email);

            r.success = false;
            if (result != "false")
            {
                r.success = true;
                r.response[0] = user.email;
                r.response[1] = result;

            }
            e.Result = r;
        }
        private void BW_Signup_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            result r = e.Result as result;
            if (r.success)
            {
                reg.Write("login", "true");
                reg.Write("EmailId", r.response[0]);
                reg.Write("UserId", r.response[1]);
                btnOk.Visible = false;
                txtEmail.Visible = false;
                btnClockin.Visible = true;
                btnClockin_Click(sender, e);
                btnParentemail.Visible = false;
                txtParentemail.Visible = false;

            }
            else
            {
                btnLogin.Visible = true;
                txtPassword.Visible = true;

            }
            progressBar1.Visible = false;
            btnClockin.Enabled = true;

        }
        private void BW_Login_DoWork(object sender, DoWorkEventArgs e)
        {
            TempUser user = e.Argument as TempUser;
            result r = new result(3);
            Attendance atd = new Attendance();
            string emailId = user.email;
            string password = user.password;
            string jsonText = atd.VerifyLogin(emailId, password);

            r.success = false;
            if (jsonText != null)
            {
                if (jsonText == "TRUE")
                {
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetLoginInfo(emailId));
                    r.success = true;
                    r.response[0] = user.email;
                    r.response[1] = dt.Rows[0]["ID"].ToString();
                    r.response[2] = dt.Rows[0]["instanceId"].ToString();
                }

            }
            e.Result = r;
        }
        private void BW_Login_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            result r = e.Result as result;
            if (r.success)
            {

                instanceId = r.response[2];
                if (reg.Read("InstanceId") == null)
                {
                    reg.Write("InstanceId", instanceId);
                }
                else
                {
                    instanceId = reg.Read("InstanceId");
                }

                reg.Write("login", "true");
                reg.Write("EmailId", r.response[0]);
                reg.Write("UserId", r.response[1]);
                btnOk.Visible = false;
                txtEmail.Visible = false;
                txtPassword.Visible = false;
                btnLogin.Visible = false;
                btnClockin.Visible = true;

                SetStatus(true);
            }
            else
            {
                MessageBox.Show("Invalid Email Id/Password", "Time & Attendance", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmail.Focus();

            }
            progressBar1.Visible = false;
            btnClockin.Enabled = true;
        }
        private void BW_Clock_DoWork(object sender, DoWorkEventArgs e)
        {
            TempUser user = e.Argument as TempUser;
            result r = new result(2);
            WebService.Attendance atd = new WebService.Attendance();
            string he = new WebClient().DownloadString(@"http://icanhazip.com").Trim();
            string address = he;
            if (hdnStatus.Text.ToLower() == "clock-in")
            {
                r.response[0] = "clock-in";
                string userId = user.userid;
                string instanceId = user.instanceid;
                DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.ClockIn(userId, address,"desktop",1,""));
                r.success = true;
                r.response[1] = dt.Rows[0]["RowId"].ToString();

                //r.response[1] = "4589";
                //r.success = true;

                //timeSpend_timer.Start();

            }
            else
            {
                r.response[0] = "clock-out";
                string userId = user.userid;
                string instanceId = user.instanceid;
                string rowId = user.rowid;
                atd.ClockOut(userId, address, Convert.ToInt32(rowId),"desktop",1,"");

                timeSpend_timer.Stop();
            }

            e.Result = r;
        }
        private void BW_Clock_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            result r = e.Result as result;
            if (r.success && r.response[0] == "clock-in")
            {
                reg.Write("RowId", r.response[1]);
                //btnClockin.Text = "Clock-out";
                hdnStatus.Text = "Clock-out";
                btnClockin.Enabled = false;
                btnClockout.Enabled = true;
            }
            else
            {
                //btnClockin.Text = "Clock-in";
                //lblClockin.Text = "Last Clock-out";
                hdnStatus.Text = "Clock-in";
                btnClockout.Enabled = false;
                btnClockin.Enabled = true;
                lblTime.Text = "00:00";
            }
            SetStatus(false);

        }

        private void BW_Setstatus_DoWork(object sender, DoWorkEventArgs e)
        {
            TempUser user = e.Argument as TempUser;
            result r = new result(6);
            string userId = user.userid;
            string AppId = reg.Read("AppId");

            WebService.Attendance atd = new WebService.Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetStatus(userId));
            AvaimaTime.AvaimaTimeZoneAPI atp = new AvaimaTime.AvaimaTimeZoneAPI();

            if (dt.Rows.Count > 0)
            {
                r.response[0] = dt.Rows[0]["lastout"].ToString();
                if (dt.Rows[0]["lastout"].ToString() == "")
                {
                    string time = atp.GetTime(Convert.ToDateTime(dt.Rows[0]["lastin"]).ToShortTimeString(), AppId);
                    r.response[1] = "Last Clock-in " + time + " on " + Convert.ToDateTime(dt.Rows[0]["lastin"]).DayOfWeek + ", " + Convert.ToDateTime(dt.Rows[0]["lastin"]).ToString("MMMM dd,yyyy");
                    r.response[2] = dt.Rows[0]["RowId"].ToString();

                    r.response[5] = dt.Rows[0]["hours"].ToString();

                    //new - get last day worked
                    //r.response[4] = "Time spent on " + Convert.ToDateTime(dt.Rows[1]["lastout"]).DayOfWeek + " (" + Convert.ToDateTime(dt.Rows[1]["lastout"]).ToString("MMMM dd,yyyy") + "):" + dt.Rows[1]["hours"].ToString();

                }
                else
                {
                    string time = atp.GetTime(Convert.ToDateTime(dt.Rows[0]["lastout"]).ToShortTimeString(), AppId);
                    r.response[1] = "Last Clock-out " + time + " on " +
                        Convert.ToDateTime(dt.Rows[0]["lastout"]).DayOfWeek + ", " +
                        Convert.ToDateTime(dt.Rows[0]["lastout"]).ToString("MMMM dd,yyyy");


                    //r.response[5] = dt.Rows[0]["hours"].ToString();

                    //new - get last day worked
                    //r.response[4] = "Time spent on" + DateTime.Today.AddDays(-1).DayOfWeek + ":" + dt.Rows[0]["hours"].ToString();
                    //r.response[4] = "Time spent on " + Convert.ToDateTime(dt.Rows[0]["lastout"]).DayOfWeek + " (" + Convert.ToDateTime(dt.Rows[0]["lastout"]).ToString("MMMM dd,yyyy") + "):" + dt.Rows[0]["hours"].ToString();

                }

                //    //Show History               
                //    gridview_History.Invoke((MethodInvoker)delegate
                //    {
                //        try
                //        {
                //            gridHistory(dt, AppId, "Fill");
                //        }
                //        catch (Exception ex)
                //        {
                //            Console.Write(ex);
                //        }
                //    });
            }
            else
            {
                //    gridview_History.Invoke((MethodInvoker)delegate
                //    {
                //        try
                //        {
                //            gridHistory(dt, AppId, "Empty");
                //        }
                //        catch (Exception ex)
                //        {
                //            Console.Write(ex);
                //        }
                //    });
                r.response[1] = "Last Clock-in NEVER";
            }

            //r.response[4] = "Time spent on " + Convert.ToDateTime(dt.Rows[1]["lastout"]).DayOfWeek + " (" + Convert.ToDateTime(dt.Rows[1]["lastout"]).ToString("MMMM dd,yyyy") + "):" + dt.Rows[1]["hours"].ToString();


            if (user.clockin)
                r.response[3] = "true";
            else
                r.response[3] = "false";
            e.Result = r;
        }

        private void BW_Setstatus_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            result r = e.Result as result;

            if (r.response[0] == "")
            {
                lblClockin.Invoke((Action)delegate
                {
                    //btnClockin.Text = "Clock-out";
                    hdnStatus.Text = "Clock-out";
                    lblClockin.Text = r.response[1];
                    //lblLastTime.Text = r.response[4];
                    lblTime.Text = r.response[5];
                    btnClockin.Enabled = false;
                    btnClockout.Enabled = true;

                    timeSpend_timer.Start();


                });
                reg.Write("RowId", r.response[2]);
            }
            else
            {
                lblClockin.Invoke((Action)delegate
                {
                    lblClockin.Text = r.response[1];
                    //lblLastTime.Text = r.response[4];
                    lblTime.Text = r.response[5];
                    //btnClockin.Text = "Clock-in";
                    hdnStatus.Text = "Clock-in";
                    btnClockin.Enabled = true;
                    btnClockout.Enabled = false;
                });
            }

            progressBar1.Invoke((Action)delegate
            {
                progressBar1.Visible = false;
                //gridview_History.Visible = true;
                //btnClockin.Enabled = true;

                //Clock
                clock_timer.Start();
                //////timeSpend_timer.Start();

                if (r.response[3] == "true")
                {
                    btnClockin_Click(sender, e);
                }
            });

        }

        private DataTable GetUserStatus(string userId)
        {
            WebService.Attendance atd = new WebService.Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetStatus(userId));
            return dt;
        }

        private void BW_Isregistered_DoWork(object sender, DoWorkEventArgs e)
        {
            TempUser user = e.Argument as TempUser;
            result r = new result(2);
            string result = "false";
            AddUser.AddUser adduser = new AddUser.AddUser();
            try
            {
                result = adduser.is_registered(user.email);
            }
            catch (WebException wEx)
            {
                MessageBox.Show(wEx.Message);
                BW_Isregistered.CancelAsync();
                return;
            }

            if (result == "false")
            {
                r.success = false;
            }
            else
            {
                r.success = true;
            }
            e.Result = r;


        }

        private void BW_Isregistered_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {


            result r = e.Result as result;
            if (r.success == true)
            {
                btnLogin.Visible = true;
                txtPassword.Visible = true;
            }
            else
            {

                txtParentemail.Visible = true;
                btnParentemail.Visible = true;
            }
            progressBar1.Visible = false;
            btnClockin.Enabled = true;
        }

        private void btnParentemail_Click(object sender, EventArgs e)
        {
            TempUser user = new TempUser
            {
                email = txtEmail.Text,
                password = "ajsdh!@#kasd",
                parent_email = txtParentemail.Text
            };
            BW_Signup.RunWorkerAsync(user);
            progressBar1.Visible = true;
            btnClockin.Enabled = false;
        }

        private string GetDiff(object signin, object signout)
        {
            DateTime start = Convert.ToDateTime(signin);
            DateTime end = (signout.ToString() != "") ? Convert.ToDateTime(signout) : DateTime.Now;
            TimeSpan ts = end.Subtract(start);
            string Output = "";

            double totalyears = (ts.TotalSeconds) / 31556926;
            double years = Math.Floor(ts.TotalSeconds / 31556926);
            //Total months - totalyears - year * sec pr year / sec pr month
            double totalmonths = (((totalyears) - years) * 31556926) / 2629743.83;
            double months = Math.Floor(totalmonths);
            //Total days - totalmonths - months * sec pr month / sec pr day
            double totaldays = (((totalmonths) - months) * 2629743.83) / 86400;
            double days = Math.Floor(totaldays);
            //Total Hours - totaldays - days * sec pr day / sec pr hour
            double totalhours = ((totaldays - days) * 86400) / 3600;
            double hours = Math.Floor(totalhours);
            //Total Minutes - totalhours - hours * sec pr hour / sec pr min
            double totalminutes = ((totalhours - hours) * 3600) / 60;
            double minutes = Math.Floor(totalminutes);
            //Total Seconds - totalminutes - minutes * sec pr minute
            double totalseconds = ((totalminutes - minutes) * 60);
            double seconds = Math.Floor(totalseconds);

            if (years > 0)
            {
                Output += years;
                Output += (years > 1) ? " years " : " year ";
            }
            if (months > 0)
            {
                Output += months;
                Output += (months > 1) ? " months " : " month ";
            }
            if (days > 0)
            {
                Output += days;
                Output += (days > 1) ? " days " : " day ";
            }
            if (hours > 0)
            {
                Output += hours;
                Output += (hours > 1) ? " hrs " : " hr ";
            }
            //if (minutes > 0)
            //{
            Output += minutes;
            Output += (minutes > 1) ? " mins " : " min ";
            //}

            return Output;
        }

        private void gridHistory(DataTable dt, string AppId, string condition)
        {
            string[] formats = {"M/d/yyyy",
                "MM/dd/yyyy",
                            "d/M/yyyy",
                "dd/MM/yyyy",
                            "yyyy/M/d",
                "yyyy/MM/dd",
                            "M-d-yyyy",
                "MM-dd-yyyy",
                            "d-M-yyyy",
                "dd-MM-yyyy",
                            "yyyy-M-d",
                "yyyy-MM-dd",
                            "M.d.yyyy",
                "MM.dd.yyyy",
                            "d.M.yyyy",
                "dd.MM.yyyy",
                            "yyyy.M.d",
                "yyyy.MM.dd",
                            "M,d,yyyy",
                "MM,dd,yyyy",
                            "d,M,yyyy",
                "dd,MM,yyyy",
                            "yyyy,M,d",
                "yyyy,MM,dd",
                            "M d yyyy",
                "MM dd yyyy",
                            "d M yyyy",
                "dd MM yyyy",
                            "yyyy M d",
                "yyyy MM dd",
                            "M/dd/yyyy",
                "MM/d/yyyy",
                "dd-MMM-yyyy"
                           };



            gridview_History.Visible = false;

            AvaimaTime.AvaimaTimeZoneAPI atp = new AvaimaTime.AvaimaTimeZoneAPI();

            //reinitialize Grid
            gridview_History.DataSource = null;
            gridview_History.Rows.Clear();
            gridview_History.Refresh();

            //Set AutoGenerateColumns False
            gridview_History.AutoGenerateColumns = false;

            //Set Columns Count
            gridview_History.ColumnCount = 6;

            //Add Columns
            gridview_History.Columns[0].Name = "Date";
            gridview_History.Columns[0].HeaderText = "";
            gridview_History.Columns[0].DataPropertyName = "instatus";

            gridview_History.Columns[1].HeaderText = "Clock In";
            gridview_History.Columns[1].Name = "lastin";
            gridview_History.Columns[1].DataPropertyName = "lastin";

            gridview_History.Columns[2].Name = "signinaddress";
            gridview_History.Columns[2].HeaderText = "Clock In Location";
            gridview_History.Columns[2].DataPropertyName = "signinaddress";

            gridview_History.Columns[3].HeaderText = "Clock Out";
            gridview_History.Columns[3].Name = "lastout";
            gridview_History.Columns[3].DataPropertyName = "lastout";

            gridview_History.Columns[4].Name = "signoutaddress";
            gridview_History.Columns[4].HeaderText = "Clock Out Location";
            gridview_History.Columns[4].DataPropertyName = "signoutaddress";

            gridview_History.Columns[5].Name = "Worked";
            gridview_History.Columns[5].HeaderText = "Worked";
            gridview_History.Columns[5].DataPropertyName = "hours";


            if (condition == "Fill")
            {
                gridview_History.DataSource = dt;

                gridview_History.Columns[0].DefaultCellStyle.Format = "ddd, MMMM dd yyyy";
                gridview_History.Columns[1].DefaultCellStyle.Format = "hh:mm tt";
                gridview_History.Columns[3].DefaultCellStyle.Format = "hh:mm tt";
                gridview_History.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                gridview_History.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                foreach (DataGridViewRow row in gridview_History.Rows)
                {
                    string lastin = row.Cells["lastin"].Value.ToString();
                    string lastout = row.Cells["lastout"].Value.ToString();

                    //Set Clockin Date
                    if (!string.IsNullOrEmpty(row.Cells["Date"].Value.ToString()))
                    {
                        row.Cells["Date"].Value = atp.GetDate(Convert.ToDateTime(lastin).ToString("MMMM dd,yyyy"), AppId);
                    }

                    //Set Clockin and Clockout Time to user-friendly readibililty
                    if (!string.IsNullOrEmpty(row.Cells["lastin"].Value.ToString()))
                        row.Cells["lastin"].Value = atp.GetTime(Convert.ToDateTime(row.Cells["lastin"].Value).ToShortTimeString(), AppId);

                    if (!string.IsNullOrEmpty(row.Cells["lastout"].Value.ToString()))
                    {
                        DateTime lt = obj.FormatDate(row.Cells["lastout"].Value.ToString());
                        //DateTime lt = DateTime.ParseExact(row.Cells["lastout"].Value.ToString(), myDateformats, CultureInfo.InvariantCulture, DateTimeStyles.None);

                        row.Cells["lastout"].Value = atp.GetTime(lt.ToShortTimeString(), AppId);
                    }
                }
            }
            else if (condition == "Empty")
            {
                gridview_History.Rows.Add(1);
                gridview_History[0, 0].Value = "Today";
                gridview_History[1, 0].Value = "No Entry";
            }
        }

        private void addEmployeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddEmployee emp_form = new AddEmployee("Add");
            emp_form.Show();
        }

        private void viewEmployeesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddEmployee emp_form = new AddEmployee("View");
            emp_form.Size = new Size(560, 400);
            emp_form.Show();
        }

        private void clock_timer_Tick(object sender, EventArgs e)
        {
            clocktick.Text = DateTime.Now.ToString("hh:mm:ss tt");
        }

        private void ManageTime_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                obj.NotifyIcon(sender);
                this.Hide();
                e.Cancel = true;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;
        }

        //Update Time spend label every second
        private void timeSpend_timer_Tick(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = GetUserStatus(reg.Read("UserId"));
                lblTime.Text = dt.Rows[0]["hours"].ToString();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void LoadUsers()
        {
            AvaimaTime.AvaimaTimeZoneAPI atp = new AvaimaTime.AvaimaTimeZoneAPI();
            Attendance atd = new Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUsers(Convert.ToInt32(reg.Read("UserId")), reg.Read("InstanceId")));

            //reinitialize Grid
            gridview_users.DataSource = null;
            gridview_users.Rows.Clear();
            gridview_users.Refresh();

            //Set AutoGenerateColumns False
            gridview_users.AutoGenerateColumns = false;

            //Set Columns Count
            gridview_users.ColumnCount = 7;

            //Add Columns
            gridview_users.Columns[0].Name = "Title";
            gridview_users.Columns[0].HeaderText = "Title";
            gridview_users.Columns[0].DataPropertyName = "Title";

            gridview_users.Columns[1].Name = "Date";
            gridview_users.Columns[1].HeaderText = "Date";
            gridview_users.Columns[1].DataPropertyName = "userid";

            gridview_users.Columns[2].Name = "lastin";
            gridview_users.Columns[2].HeaderText = "Clock-in";
            gridview_users.Columns[2].DataPropertyName = "lastin";

            gridview_users.Columns[3].HeaderText = "Work Status";
            gridview_users.Columns[3].Name = "status";
            gridview_users.Columns[3].DataPropertyName = "status";

            gridview_users.Columns[4].Name = "id";
            gridview_users.Columns[4].DataPropertyName = "ID";
            gridview_users.Columns[4].Visible = false;

            gridview_users.Columns[5].Name = "rowID";
            gridview_users.Columns[5].DataPropertyName = "rowID";
            gridview_users.Columns[5].Visible = false;

            DataGridViewLinkColumn LinkCell;

            LinkCell = new DataGridViewLinkColumn();
            LinkCell.DataPropertyName = "lastout";
            LinkCell.Name = "clockinaction";
            LinkCell.HeaderText = "";
            LinkCell.LinkColor = Color.Green;
            gridview_users.Columns.Add(LinkCell);

            LinkCell = new DataGridViewLinkColumn();
            LinkCell.DataPropertyName = "InstanceId";
            LinkCell.Name = "action";
            LinkCell.HeaderText = "";
            LinkCell.LinkColor = Color.Red;
            gridview_users.Columns.Add(LinkCell);

            usersDt = dt;
            usersRowsCount = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if ((Convert.ToString(dr["status"]) == "Present")
                        || (Convert.ToString(dr["status"]) == "Absent"))
                {
                    usersRowsCount++;
                }
            }
            if (!bgWorkerUserCount.IsBusy)
            bgWorkerUserCount.RunWorkerAsync();

           

            if (dt.Rows.Count > 0)
            {
                gridview_users.Visible = btnAddUser.Visible = true;
                nouser_panel.Visible = false;

                DataRow[] notclockin = dt.Select("status = ''");
                DataRow[] absent = dt.Select("status = 'Absent'");
                DataRow[] clockin = dt.Select("status = 'Present'");
                DataRow[] clockedout = dt.Select("status = 'Clocked out'");
                DataRow[] error = dt.Select("status= 'Error'");
                DataTable records = new DataTable();

                DataTable dd = dt.Clone();

                foreach (DataRow row in notclockin)
                {
                    dd.ImportRow(row);
                }
                foreach (DataRow row in absent)
                {
                    dd.ImportRow(row);
                }
                foreach (DataRow row in clockedout)
                {
                    dd.ImportRow(row);
                }
                foreach (DataRow row in clockin)
                {
                    dd.ImportRow(row);
                }
                foreach (DataRow row in error)
                {
                    dd.ImportRow(row);
                }


                gridview_users.DataSource = dd;
                gridview_users.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                gridview_users.Columns[0].Width = 110;

                foreach (DataGridViewRow row in gridview_users.Rows)
                {
                    string DateTime = row.Cells["lastin"].Value.ToString();
                    if (!string.IsNullOrEmpty(DateTime))
                        row.Cells["lastin"].Value = atp.GetTime(obj.FormatDate(DateTime).ToShortTimeString(), reg.Read("AppId"));

                    row.Cells["Date"].Value = "Today";

                    if (row.Cells["status"].Value.ToString() == "Present")
                    {
                        row.Cells["action"].Style.ForeColor = Color.Red;
                        row.Cells["action"].ToolTipText = "Clock-out";
                        row.Cells["action"].Value = "Clock-out";
                    }
                    else if (row.Cells["status"].Value.ToString() == "")
                    {
                        row.Cells["action"].Style.ForeColor = Color.Red;
                        row.Cells["action"].ToolTipText = "Mark as Absent";
                        row.Cells["action"].Value = "Mark as Absent";

                        row.Cells["clockinaction"].Style.ForeColor = Color.Green;
                        row.Cells["clockinaction"].ToolTipText = "Clock-in";
                        row.Cells["clockinaction"].Value = "Clock-in";
                    }
                    else if (row.Cells["status"].Value.ToString() == "Error")   //New 
                    {
                        row.Cells["action"].Style.ForeColor = Color.Red;
                        row.Cells["action"].ToolTipText = "User not clocked-out";
                        row.Cells["action"].Value = "User not clocked-out";
                    }
                    else
                    {
                        row.Cells["action"].Value = "";
                        row.Cells["clockinaction"].Value = "";
                        
                    }
                }
            }
            else
            {
                gridview_users.Visible = btnAddUser.Visible = false;
                nouser_panel.Visible = true;
            }
        }


        private DataGridViewLinkColumn Cell(string type)
        {
            DataGridViewLinkCell LinkCell = new DataGridViewLinkCell();
            DataGridViewLinkColumn dgvLink = new DataGridViewLinkColumn();

            if (type == "clockin")
            {
                LinkCell.ToolTipText = "Mark as Absent";


                dgvLink.UseColumnTextForLinkValue = true;
                dgvLink.LinkBehavior = LinkBehavior.SystemDefault;
                dgvLink.HeaderText = "Link Data";
                dgvLink.Name = "SiteName";
                dgvLink.LinkColor = Color.Blue;
                dgvLink.TrackVisitedState = true;
                dgvLink.Text = "Clock-out";
                dgvLink.UseColumnTextForLinkValue = true;
            }

            if (type == "notclockin")
            {
                dgvLink.UseColumnTextForLinkValue = true;
                dgvLink.LinkBehavior = LinkBehavior.SystemDefault;
                dgvLink.HeaderText = "Link Data";
                dgvLink.Name = "SiteName";
                dgvLink.LinkColor = Color.Blue;
                dgvLink.TrackVisitedState = true;
                dgvLink.Text = "Clock-in";
                dgvLink.UseColumnTextForLinkValue = true;
            }

            if (type == "absent")
            {
                dgvLink.UseColumnTextForLinkValue = true;
                dgvLink.LinkBehavior = LinkBehavior.SystemDefault;
                dgvLink.HeaderText = "Link Data";
                dgvLink.Name = "SiteName";
                dgvLink.LinkColor = Color.Blue;
                dgvLink.TrackVisitedState = true;
                dgvLink.Text = "Mark as Absent";
                dgvLink.UseColumnTextForLinkValue = true;
            }

            if (type == "blank")
            {
                dgvLink.UseColumnTextForLinkValue = true;
                dgvLink.LinkBehavior = LinkBehavior.SystemDefault;
                dgvLink.HeaderText = "Link Data";
                dgvLink.Name = "SiteName";
                dgvLink.LinkColor = Color.Blue;
                dgvLink.TrackVisitedState = true;
                dgvLink.Text = "";
                dgvLink.UseColumnTextForLinkValue = true;
            }

            return dgvLink;

        }

        private void gridview_users_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int columnIndex = gridview_users.CurrentCell.ColumnIndex;
            string columnName = gridview_users.Columns[columnIndex].Name;
            string columnValue = gridview_users.Rows[gridview_users.CurrentRow.Index].Cells[columnName].Value.ToString();
            int userid = Convert.ToInt32(gridview_users.Rows[gridview_users.CurrentRow.Index].Cells["ID"].Value.ToString());
            Hashtable ht = new Hashtable();
            bool result = false;

            if (columnName == "action")
            {
                if (!string.IsNullOrEmpty(columnValue))
                {
                    if (columnValue == "Mark as Absent")
                    {
                        ht["type"] = "absent";
                    }

                    if (columnValue == "Clock-out" || columnValue == "User not clocked-out")
                    {
                        string address = new WebClient().DownloadString(@"http://icanhazip.com").Trim();
                        ht["type"] = "clockout";
                        ht["outaddress"] = address;
                        ht["rowId"] = Convert.ToInt32(gridview_users.Rows[gridview_users.CurrentRow.Index].Cells["rowID"].Value.ToString());
                    }

                    ht["userid"] = userid;
                    result = ChangeStatus(ht);

                    if (result)
                    {
                        MessageBox.Show("User " + columnValue);
                        LoadUsers();
                    }
                    else
                        MessageBox.Show("User could not be " + columnValue + " successfully!");
                }
            }

            if (columnName == "clockinaction")
            {
                if (!string.IsNullOrEmpty(columnValue))
                {
                    if (columnValue == "Clock-in")
                    {
                        string address = new WebClient().DownloadString(@"http://icanhazip.com").Trim();
                        ht["type"] = "clockin";
                        ht["inaddress"] = address;
                    }

                    ht["userid"] = userid;
                    result = ChangeStatus(ht);

                    if (result)
                    {
                        MessageBox.Show("User " + columnValue + " successfully!");
                        LoadUsers();
                    }
                    else
                        MessageBox.Show("User could not be " + columnValue);
                }
            }
        }

        private bool ChangeStatus(Hashtable ht)
        {
            Attendance atd = new Attendance();
            bool result = false;
            string res = "";

            if (ht["type"].ToString() == "absent")
            {
                res = atd.MarkAbsent(Convert.ToInt32(ht["userid"].ToString()), "");
            }
            if (ht["type"].ToString() == "clockout")
            {
                res = atd.ClockOut(ht["userid"].ToString(), ht["outaddress"].ToString(), Convert.ToInt32(ht["rowId"].ToString()), "desktop",2,"Clock-out by admin");
            }
            if (ht["type"].ToString() == "clockin")
            {
                res = atd.ClockIn(ht["userid"].ToString(), ht["inaddress"].ToString(),"desktop",2,"Clock-in by admin" );
                //ht["@ppid"] = userid;
                //ht["@lastin"] = Convert.ToDateTime(DateTime.Now);
                //ht["@inaddress"] = inaddress;
                //ht["@in_location"] = in_location;
                //ht["@comment"] = comment;
                //ht["@logIn_device"] = logIn_device;
            }

            result = Convert.ToBoolean(res);

            return result;

        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            this.ParentForm.Hide();
            MyApplication HomePage = new MyApplication("adduser");
            HomePage.Show();


            //gridview_users.Visible = btnAddUser.Visible = lbl_nouser.Visible = false;
            //nouser_panel.Visible = true;
        }

        private void btn_adduser_Click(object sender, EventArgs e)
        {
            if (txtEmailAddress.Text == "")
            {
                MessageBox.Show("Please enter Email Id", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailAddress.Focus();
                return;
            }
            if (txtFname.Text == "")
            {
                MessageBox.Show("Please enter Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFname.Focus();
                return;
            }

            if (obj.ValidateEmail(txtEmailAddress.Text))
            {
                string email = txtEmailAddress.Text;
                string fname = txtFname.Text;
                string lname = txtLname.Text;

                string Message = obj.AddUser(fname, lname, email);

                //test
                //string Message = "Employee added successfully";

                MessageBox.Show(Message, "Information", MessageBoxButtons.OK);

                if (Message.Contains("successfully"))
                {
                    LoadUsers();
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please provide valid Email Address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailAddress.Focus();
            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                Thread.Sleep(5000);
                try
                {
                    int tempUsersCount = 0;
                    Attendance atd = new Attendance();
                    DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUsers(Convert.ToInt32(reg.Read("UserId")), reg.Read("InstanceId")));
                    foreach (DataRow dr in dt.Rows)
                    {
                        if ((Convert.ToString(dr["status"]) == "Present")
                            || (Convert.ToString(dr["status"]) == "Absent"))
                        {
                            tempUsersCount++;
                        }
                    }

                    if (tempUsersCount != usersRowsCount)
                    {
                        if (tempUsersCount > usersRowsCount)
                        {
                            //user signed in
                            List<string> tempPreviousPresentUsers = new List<string>();
                            foreach (DataRow dr in usersDt.Rows)
                            {
                                if (Convert.ToString(dr["status"]) == "Present")
                                {
                                    tempPreviousPresentUsers.Add(Convert.ToString(dr["Title"]));
                                }
                            }
                            List<string> tempNowPresentUsers = new List<string>();
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (Convert.ToString(dr["status"]) == "Present")
                                {
                                    tempNowPresentUsers.Add(Convert.ToString(dr["Title"]));
                                }
                            }

                            string newLoggedInUsers =
                                string.Join(", ", tempNowPresentUsers.Except(tempPreviousPresentUsers).ToList());

                            usersDt = dt;
                            usersRowsCount = tempUsersCount;
                            Notification frm = new Notification();
                            frm.lblMsgText = newLoggedInUsers + " logged in.";
                            frm.Show();
                            Thread.Sleep(5000);
                            frm.Close();
                            frm.Dispose();
                            //LoadUsers();
                        }
                        if (tempUsersCount < usersRowsCount)
                        {
                            //user signed out
                            List<string> tempPreviousAbsentUsers = new List<string>();
                            foreach (DataRow dr in usersDt.Rows)
                            {
                                if (Convert.ToString(dr["status"]) == "Present")
                                {
                                    tempPreviousAbsentUsers.Add(Convert.ToString(dr["Title"]));
                                }
                            }
                            List<string> tempNowAbsentUsers = new List<string>();
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (Convert.ToString(dr["status"]) == "Present")
                                {
                                    tempNowAbsentUsers.Add(Convert.ToString(dr["Title"]));
                                }
                            }

                            string newLoggedOutUsers =
                                string.Join(", ", tempPreviousAbsentUsers.Except(tempNowAbsentUsers).ToList());

                            usersDt = dt;
                            usersRowsCount = tempUsersCount;
                            Notification frm = new Notification();
                            frm.lblMsgText = newLoggedOutUsers + " logged out.";
                            frm.Show();
                            Thread.Sleep(5000);
                            frm.Close();
                            frm.Dispose();
                            //MainMenu_UserControl mainMenu = new MainMenu_UserControl();
                            //main_panel.Controls.Add(mainMenu);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }


}


public class result
{
    public bool success { get; set; }
    public string[] response { get; set; }
    public result(int elements)
    {
        response = new string[elements];
    }

}


public class TempUser
{
    public string email { get; set; }
    public string parent_email { get; set; }
    public string password { get; set; }
    public string userid { get; set; }
    public string instanceid { get; set; }
    public string rowid { get; set; }
    public bool clockin { get; set; }

}

