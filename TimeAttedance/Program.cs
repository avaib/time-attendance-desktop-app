﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using IWshRuntimeLibrary;
using System.Reflection;
using Microsoft.Win32;
using System.Linq;
using System.Data;
using System.Drawing;

namespace TimeAttedance
{
    static class Program
    {
        public static NotifyIcon m_notifyIcon;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        static ContextMenu NotifyMenu()
        {
            ContextMenu strip = new ContextMenu();
            strip.MenuItems.Add(new MenuItem("Exit"));
            return strip;
        }


        [STAThread]
        static void Main()
        {
            //test
            //Application.Run(new AddEmployee("View"));
            //Application.Run(new AdminRights());
            //Application.Run(new Settings());

            try
            {
                if (checkInstalled("TimeAndAttendance"))
                {
                    MessageBox.Show("Application already running", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //Process current = Process.GetCurrentProcess();
                //Process[] result = Process.GetProcessesByName(current.ProcessName);
                Process[] result = Process.GetProcessesByName("TimeAttedance");
                if (result.Length > 0)
                {
                    MessageBox.Show("Application already running", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    System.Environment.Exit(0);
                }

                Registery reg = new Registery();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //Icon Check
                string dirIcon = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\login.ico";

                if (!System.IO.File.Exists(dirIcon))
                {
                    dirIcon = (Directory.GetParent(System.IO.Path.GetDirectoryName(Application.ExecutablePath)) + @"login.ico").ToString();
                    Path.GetFullPath(Path.Combine(System.IO.Path.GetDirectoryName(Application.ExecutablePath), @"..\"));
                }
                
                m_notifyIcon = new NotifyIcon();
                m_notifyIcon.Text = "Time and Attendance";
                m_notifyIcon.Icon = new System.Drawing.Icon(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\login.ico");

                m_notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                m_notifyIcon.BalloonTipTitle = "Time and Attendance";
                m_notifyIcon.BalloonTipText = "Your application is running." +
                                            Environment.NewLine;
                m_notifyIcon.ShowBalloonTip(5000);



                //NotifyIcon Menu
                ContextMenu contextmenu1 = new ContextMenu();
                contextmenu1.MenuItems.Add("Home", ProceedToHome);
                contextmenu1.MenuItems.Add("Logout", LogoutCurrentUser);
                contextmenu1.MenuItems.Add("Exit", ExitApplication);

                m_notifyIcon.ContextMenu = contextmenu1;

                m_notifyIcon.Visible = true;


                //If RememberMe checkbox checked
                LoadForm(reg);

            }
            catch (Exception ex)
            {
                string path = Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLog.txt";
                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    sw.Write(string.Format("Message: {0}<br />{1}StackTrace :{2}{1}Date :{3}{1}-----------------------------------------------------------------------------{1}", ex.ToString(), Environment.NewLine, ex.StackTrace, DateTime.Now.ToString()));
                }

                //Loading frm = new Loading();
                //frm.ShowDialog();

                //--Genral Mesaage--//
                MessageBox.Show("An unexpected error has occurred! Sorry for the inconvinience.\n Please login to www.avaima.com to continue.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //   System.Environment.Exit(0);
            }
        }

        private static void LogoutCurrentUser(object sender, EventArgs e)
        {
            foreach (Form frm in Application.OpenForms)
                frm.Hide();

            Registery reg = new Registery();
            reg.Write("login", "false");
            reg.Write("UserName", "");
            reg.Write("Password", "");

            Login form = new Login();
            form.Show();
        }

        private static void ProceedToHome(object sender, EventArgs e)
        {
            foreach (Form form in Application.OpenForms)
                form.Hide();

            MyApplication homeForm = new MyApplication();
            homeForm.Show();
        }

        public static void LoadForm(Registery reg)
        {
            reg.IsConnected();

            if ((reg.Read("UserName") != null && reg.Read("Password") != null) && (reg.Read("UserName") != "" && reg.Read("Password") != ""))
            {
                Application.Run(new MyApplication());
            }
            else
            {
                Application.Run(new Login());
            }
        }

        public static bool checkInstalled(string softwareName)
        {
            string registryKey = @"SOFTWARE\TimeAndAttendance";
            var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\") ??
                   Registry.CurrentUser.OpenSubKey(
                       @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall");

            if (key == null)
                return false;

            return key.GetSubKeyNames()
                .Select(keyName => key.OpenSubKey(keyName))
                .Select(subkey => subkey.GetValue("DisplayName") as string)
                .Any(displayName => displayName != null && displayName.Contains(softwareName));
        }

        private static void ExitApplication(object sender, EventArgs e)
        {
            m_notifyIcon.Visible = false;
            m_notifyIcon.Dispose();
            Application.Exit();
        }




    }
}
