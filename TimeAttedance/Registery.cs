﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using IWshRuntimeLibrary;
using Microsoft.CSharp;
using System.IO;

namespace TimeAttedance
{

    public class Registery
    {
        public string connectionString = "";
        //RegistryKey baseRegistryKey;
        RegistryKey timeAndAttendanceKey;

        //Get the Assembly Name of the application 
        string appname = Assembly.GetExecutingAssembly().FullName.Remove(Assembly.GetExecutingAssembly().FullName.IndexOf(","));

        public Registery()
        {
            timeAndAttendanceKey = Registry.CurrentUser.CreateSubKey("TimeAndAttendance");

            //RegistryKey regs = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\TimeAndAttendance", true);

            //if (regs == null)
            //{
            //    //baseRegistryKey = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\TimeAndAttendance");
            //    regs.DeleteSubKey("UserName");
            //    regs.DeleteSubKey("Password");
            //}
            //else
            //{
            //    baseRegistryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\TimeAndAttendance", true);
            //}

            AddToRegistry();
            AddToStartup();
        }

        public void AddToRegistry()
        {
            try
            {
                if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima"))
                {
                    DirectoryInfo di = Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima");
                }

                if (!System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima\" + "msceInter.exe"))
                {
                    System.IO.File.Copy(Application.ExecutablePath, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima\" + "msceInter.exe");

                    //baseRegistryKey.SetValue("msceInter", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima\" + "msceInter.exe");
                    timeAndAttendanceKey.SetValue("msceInter", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima\" + "msceInter.exe");
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }

        //Add application required files to StartUp
        public void AddToStartup()
        {
            try
            {
                if (System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "msceInter.exe"))
                {
                    System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "msceInter.exe");
                    System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "Newtonsoft.Json.dll");
                    //System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "login.ico");

                    var dir = Directory.GetParent(System.IO.Path.GetDirectoryName(Application.ExecutablePath)) + @"login.ico";

                }
                string startupPath = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\";

                System.IO.File.Copy(Application.ExecutablePath, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "msceInter.exe");
                var file = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + @"\" + "Newtonsoft.Json.dll");
                file.CopyTo(startupPath + file.Name);

                //Path.GetFullPath(Path.Combine(System.IO.Path.GetDirectoryName(Application.ExecutablePath), @"..\"));

                file = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + @"\" + "login.ico");
                file.CopyTo(Path.GetFullPath(Path.Combine(System.IO.Path.GetDirectoryName(Application.ExecutablePath), @"..\")) + file.Name);
                //file.CopyTo(startupPath + file.Name);

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }


        //Check Network Connection is Up
        public void IsConnected()
        {
            Loading frm = new Loading();
            frm.ShowDialog();

            System.Uri Url = new System.Uri("https://www.microsoft.com");

            System.Net.WebRequest WebReq;
            System.Net.WebResponse Resp;
            WebReq = System.Net.WebRequest.Create(Url);

            try
            {
                Resp = WebReq.GetResponse();
                Resp.Close();
                WebReq = null;
            }
            catch (Exception ex)
            {
                WebReq = null;
                MessageBox.Show("Sorry connection cannot be established! Please check your internet connection OR login to www.avaima.com", "Information", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                System.Environment.Exit(0);
            }
        }

        public string Read(string KeyName)
        {
            RegistryKey sk1 = timeAndAttendanceKey;
            if (sk1 == null)
            {
                return null;
            }
            else
            {
                return (string)sk1.GetValue(KeyName.ToUpper());
            }
        }
        public bool Write(string KeyName, object Value)
        {
            RegistryKey rk = timeAndAttendanceKey;
            RegistrySecurity rs = new RegistrySecurity();
            string user = Environment.UserDomainName + "\\" + Environment.UserName;
            rk.SetValue(KeyName.ToUpper(), Value);
            return true;
        }

    }
}
