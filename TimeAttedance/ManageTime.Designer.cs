﻿namespace TimeAttedance
{
    partial class ManageTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageTime));
            this.btnClockin = new System.Windows.Forms.Button();
            this.lblClockin = new System.Windows.Forms.Label();
            this.msLogout = new System.Windows.Forms.MenuStrip();
            this.addEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewEmployeesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.BW_Signup = new System.ComponentModel.BackgroundWorker();
            this.BW_Login = new System.ComponentModel.BackgroundWorker();
            this.BW_Clock = new System.ComponentModel.BackgroundWorker();
            this.BW_Setstatus = new System.ComponentModel.BackgroundWorker();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtParentemail = new System.Windows.Forms.TextBox();
            this.btnParentemail = new System.Windows.Forms.Button();
            this.BW_Isregistered = new System.ComponentModel.BackgroundWorker();
            this.gridview_History = new System.Windows.Forms.DataGridView();
            this.clocktick = new System.Windows.Forms.Label();
            this.clock_timer = new System.Windows.Forms.Timer(this.components);
            this.btnClockout = new System.Windows.Forms.Button();
            this.timeSpend_timer = new System.Windows.Forms.Timer(this.components);
            this.lblTime = new System.Windows.Forms.Label();
            this.hdnStatus = new System.Windows.Forms.Label();
            this.gridview_users = new System.Windows.Forms.DataGridView();
            this.btnAddUser = new System.Windows.Forms.Button();
            this.nouser_panel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmailAddress = new System.Windows.Forms.TextBox();
            this.txtLname = new System.Windows.Forms.TextBox();
            this.txtFname = new System.Windows.Forms.TextBox();
            this.btn_adduser = new System.Windows.Forms.Button();
            this.lbl_nouser = new System.Windows.Forms.Label();
            this.bgWorkerUserCount = new System.ComponentModel.BackgroundWorker();
            this.msLogout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridview_History)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridview_users)).BeginInit();
            this.nouser_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClockin
            // 
            this.btnClockin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClockin.Location = new System.Drawing.Point(27, 28);
            this.btnClockin.Name = "btnClockin";
            this.btnClockin.Size = new System.Drawing.Size(92, 34);
            this.btnClockin.TabIndex = 0;
            this.btnClockin.Text = "Clock-in";
            this.btnClockin.UseVisualStyleBackColor = true;
            this.btnClockin.Click += new System.EventHandler(this.btnClockin_Click);
            // 
            // lblClockin
            // 
            this.lblClockin.AutoSize = true;
            this.lblClockin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClockin.Location = new System.Drawing.Point(249, 28);
            this.lblClockin.Name = "lblClockin";
            this.lblClockin.Size = new System.Drawing.Size(108, 13);
            this.lblClockin.TabIndex = 1;
            this.lblClockin.Text = "Last Clock-in NEVER";
            // 
            // msLogout
            // 
            this.msLogout.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEmployeeToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.msLogout.Location = new System.Drawing.Point(0, 0);
            this.msLogout.Name = "msLogout";
            this.msLogout.Size = new System.Drawing.Size(588, 24);
            this.msLogout.TabIndex = 6;
            this.msLogout.Text = "Logout";
            this.msLogout.Visible = false;
            // 
            // addEmployeeToolStripMenuItem
            // 
            this.addEmployeeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.viewEmployeesToolStripMenuItem});
            this.addEmployeeToolStripMenuItem.Name = "addEmployeeToolStripMenuItem";
            this.addEmployeeToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.addEmployeeToolStripMenuItem.Text = "Employees";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.addToolStripMenuItem.Text = "Add Employee";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addEmployeesToolStripMenuItem_Click);
            // 
            // viewEmployeesToolStripMenuItem
            // 
            this.viewEmployeesToolStripMenuItem.Name = "viewEmployeesToolStripMenuItem";
            this.viewEmployeesToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.viewEmployeesToolStripMenuItem.Text = "View Employees";
            this.viewEmployeesToolStripMenuItem.Click += new System.EventHandler(this.viewEmployeesToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(56, 133);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(207, 20);
            this.txtEmail.TabIndex = 7;
            this.txtEmail.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(265, 133);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(68, 20);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Visible = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(56, 186);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(207, 20);
            this.txtPassword.TabIndex = 8;
            this.txtPassword.Visible = false;
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(265, 186);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(68, 20);
            this.btnLogin.TabIndex = 9;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Visible = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // BW_Signup
            // 
            this.BW_Signup.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_Signup_DoWork);
            this.BW_Signup.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BW_Signup_RunWorkerCompleted);
            // 
            // BW_Login
            // 
            this.BW_Login.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_Login_DoWork);
            this.BW_Login.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BW_Login_RunWorkerCompleted);
            // 
            // BW_Clock
            // 
            this.BW_Clock.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_Clock_DoWork);
            this.BW_Clock.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BW_Clock_RunWorkerCompleted);
            // 
            // BW_Setstatus
            // 
            this.BW_Setstatus.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_Setstatus_DoWork);
            this.BW_Setstatus.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BW_Setstatus_RunWorkerCompleted);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(42, 415);
            this.progressBar1.MarqueeAnimationSpeed = 40;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(497, 19);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 10;
            this.progressBar1.Value = 5;
            this.progressBar1.Visible = false;
            // 
            // txtParentemail
            // 
            this.txtParentemail.Location = new System.Drawing.Point(56, 159);
            this.txtParentemail.Name = "txtParentemail";
            this.txtParentemail.Size = new System.Drawing.Size(207, 20);
            this.txtParentemail.TabIndex = 11;
            this.txtParentemail.Visible = false;
            // 
            // btnParentemail
            // 
            this.btnParentemail.Location = new System.Drawing.Point(265, 157);
            this.btnParentemail.Name = "btnParentemail";
            this.btnParentemail.Size = new System.Drawing.Size(68, 23);
            this.btnParentemail.TabIndex = 12;
            this.btnParentemail.Text = "OK";
            this.btnParentemail.UseVisualStyleBackColor = true;
            this.btnParentemail.Visible = false;
            this.btnParentemail.Click += new System.EventHandler(this.btnParentemail_Click);
            // 
            // BW_Isregistered
            // 
            this.BW_Isregistered.WorkerSupportsCancellation = true;
            this.BW_Isregistered.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_Isregistered_DoWork);
            this.BW_Isregistered.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BW_Isregistered_RunWorkerCompleted);
            // 
            // gridview_History
            // 
            this.gridview_History.AllowUserToAddRows = false;
            this.gridview_History.AllowUserToDeleteRows = false;
            this.gridview_History.AllowUserToResizeColumns = false;
            this.gridview_History.AllowUserToResizeRows = false;
            this.gridview_History.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.gridview_History.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridview_History.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridview_History.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridview_History.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.gridview_History.Location = new System.Drawing.Point(7, 102);
            this.gridview_History.Name = "gridview_History";
            this.gridview_History.ReadOnly = true;
            this.gridview_History.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gridview_History.RowHeadersVisible = false;
            this.gridview_History.RowHeadersWidth = 30;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Gray;
            this.gridview_History.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gridview_History.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.gridview_History.Size = new System.Drawing.Size(408, 234);
            this.gridview_History.TabIndex = 13;
            this.gridview_History.Visible = false;
            // 
            // clocktick
            // 
            this.clocktick.AutoSize = true;
            this.clocktick.Location = new System.Drawing.Point(525, 6);
            this.clocktick.Name = "clocktick";
            this.clocktick.Size = new System.Drawing.Size(33, 13);
            this.clocktick.TabIndex = 14;
            this.clocktick.Text = "clock";
            // 
            // clock_timer
            // 
            this.clock_timer.Tick += new System.EventHandler(this.clock_timer_Tick);
            // 
            // btnClockout
            // 
            this.btnClockout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClockout.Location = new System.Drawing.Point(125, 28);
            this.btnClockout.Name = "btnClockout";
            this.btnClockout.Size = new System.Drawing.Size(98, 34);
            this.btnClockout.TabIndex = 15;
            this.btnClockout.Text = "Clock-out";
            this.btnClockout.UseVisualStyleBackColor = true;
            this.btnClockout.Click += new System.EventHandler(this.btnClockin_Click);
            // 
            // timeSpend_timer
            // 
            this.timeSpend_timer.Interval = 1000;
            this.timeSpend_timer.Tick += new System.EventHandler(this.timeSpend_timer_Tick);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(70, 74);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(90, 25);
            this.lblTime.TabIndex = 16;
            this.lblTime.Text = "00:00:00";
            // 
            // hdnStatus
            // 
            this.hdnStatus.AutoSize = true;
            this.hdnStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hdnStatus.Location = new System.Drawing.Point(24, 65);
            this.hdnStatus.Name = "hdnStatus";
            this.hdnStatus.Size = new System.Drawing.Size(44, 13);
            this.hdnStatus.TabIndex = 17;
            this.hdnStatus.Text = "clock-in";
            this.hdnStatus.Visible = false;
            // 
            // gridview_users
            // 
            this.gridview_users.AllowUserToAddRows = false;
            this.gridview_users.AllowUserToDeleteRows = false;
            this.gridview_users.AllowUserToResizeColumns = false;
            this.gridview_users.AllowUserToResizeRows = false;
            this.gridview_users.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridview_users.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridview_users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridview_users.Cursor = System.Windows.Forms.Cursors.Default;
            this.gridview_users.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.gridview_users.Location = new System.Drawing.Point(42, 133);
            this.gridview_users.Name = "gridview_users";
            this.gridview_users.ReadOnly = true;
            this.gridview_users.RowHeadersVisible = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Gray;
            this.gridview_users.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.gridview_users.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridview_users.Size = new System.Drawing.Size(497, 282);
            this.gridview_users.TabIndex = 19;
            this.gridview_users.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridview_users_CellClick);
            // 
            // btnAddUser
            // 
            this.btnAddUser.Location = new System.Drawing.Point(464, 104);
            this.btnAddUser.Name = "btnAddUser";
            this.btnAddUser.Size = new System.Drawing.Size(75, 23);
            this.btnAddUser.TabIndex = 20;
            this.btnAddUser.Text = "Add User(s)";
            this.btnAddUser.UseVisualStyleBackColor = true;
            this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
            // 
            // nouser_panel
            // 
            this.nouser_panel.Controls.Add(this.label3);
            this.nouser_panel.Controls.Add(this.label2);
            this.nouser_panel.Controls.Add(this.label1);
            this.nouser_panel.Controls.Add(this.txtEmailAddress);
            this.nouser_panel.Controls.Add(this.txtLname);
            this.nouser_panel.Controls.Add(this.txtFname);
            this.nouser_panel.Controls.Add(this.btn_adduser);
            this.nouser_panel.Controls.Add(this.lbl_nouser);
            this.nouser_panel.Location = new System.Drawing.Point(63, 122);
            this.nouser_panel.Name = "nouser_panel";
            this.nouser_panel.Size = new System.Drawing.Size(462, 293);
            this.nouser_panel.TabIndex = 21;
            this.nouser_panel.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Last Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "First Name";
            // 
            // txtEmailAddress
            // 
            this.txtEmailAddress.Location = new System.Drawing.Point(145, 129);
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.txtEmailAddress.Size = new System.Drawing.Size(168, 20);
            this.txtEmailAddress.TabIndex = 24;
            // 
            // txtLname
            // 
            this.txtLname.Location = new System.Drawing.Point(145, 103);
            this.txtLname.Name = "txtLname";
            this.txtLname.Size = new System.Drawing.Size(168, 20);
            this.txtLname.TabIndex = 23;
            // 
            // txtFname
            // 
            this.txtFname.Location = new System.Drawing.Point(145, 77);
            this.txtFname.Name = "txtFname";
            this.txtFname.Size = new System.Drawing.Size(168, 20);
            this.txtFname.TabIndex = 22;
            // 
            // btn_adduser
            // 
            this.btn_adduser.Location = new System.Drawing.Point(238, 155);
            this.btn_adduser.Name = "btn_adduser";
            this.btn_adduser.Size = new System.Drawing.Size(75, 23);
            this.btn_adduser.TabIndex = 21;
            this.btn_adduser.Text = "Add User";
            this.btn_adduser.UseVisualStyleBackColor = true;
            this.btn_adduser.Click += new System.EventHandler(this.btn_adduser_Click);
            // 
            // lbl_nouser
            // 
            this.lbl_nouser.AutoSize = true;
            this.lbl_nouser.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.lbl_nouser.Location = new System.Drawing.Point(140, 35);
            this.lbl_nouser.Name = "lbl_nouser";
            this.lbl_nouser.Size = new System.Drawing.Size(173, 25);
            this.lbl_nouser.TabIndex = 0;
            this.lbl_nouser.Text = "No data available !";
            // 
            // bgWorkerUserCount
            // 
            this.bgWorkerUserCount.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
            // 
            // ManageTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 471);
            this.Controls.Add(this.nouser_panel);
            this.Controls.Add(this.btnAddUser);
            this.Controls.Add(this.gridview_users);
            this.Controls.Add(this.hdnStatus);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.btnClockout);
            this.Controls.Add(this.clocktick);
            this.Controls.Add(this.gridview_History);
            this.Controls.Add(this.btnParentemail);
            this.Controls.Add(this.txtParentemail);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.msLogout);
            this.Controls.Add(this.lblClockin);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnClockin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ManageTime";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Time";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ManageTime_FormClosing);
            this.Load += new System.EventHandler(this.ManageTime_Load);
            this.msLogout.ResumeLayout(false);
            this.msLogout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridview_History)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridview_users)).EndInit();
            this.nouser_panel.ResumeLayout(false);
            this.nouser_panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClockin;
        private System.Windows.Forms.Label lblClockin;
        private System.Windows.Forms.MenuStrip msLogout;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.ComponentModel.BackgroundWorker BW_Signup;
        private System.ComponentModel.BackgroundWorker BW_Login;
        private System.ComponentModel.BackgroundWorker BW_Clock;
        private System.ComponentModel.BackgroundWorker BW_Setstatus;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtParentemail;
        private System.Windows.Forms.Button btnParentemail;
        private System.ComponentModel.BackgroundWorker BW_Isregistered;
        private System.Windows.Forms.DataGridView gridview_History;
        private System.Windows.Forms.ToolStripMenuItem addEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewEmployeesToolStripMenuItem;
        private System.Windows.Forms.Label clocktick;
        private System.Windows.Forms.Timer clock_timer;
        private System.Windows.Forms.Button btnClockout;
        private System.Windows.Forms.Timer timeSpend_timer;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label hdnStatus;
        private System.Windows.Forms.DataGridView gridview_users;
        private System.Windows.Forms.Button btnAddUser;
        private System.Windows.Forms.Panel nouser_panel;
        private System.Windows.Forms.Button btn_adduser;
        private System.Windows.Forms.Label lbl_nouser;
        private System.Windows.Forms.TextBox txtEmailAddress;
        private System.Windows.Forms.TextBox txtLname;
        private System.Windows.Forms.TextBox txtFname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker bgWorkerUserCount;
    }
}