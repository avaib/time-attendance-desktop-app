﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.WebService;

namespace TimeAttedance
{
    public partial class Settings : Form
    {
        Main obj;
        Registery reg = new Registery();
        Attendance atd;

        public Settings()
        {
            InitializeComponent();
            LoadData();
        }
               
        private void LoadData()
        {
            txtfullname.Text = reg.Read("Title");
            Loadtimzone();
            Loadweekends();
        }

        private void Loadtimzone()
        {
            obj = new Main();

            int maxlength = 0;
            foreach (string str in obj.TimeZone)
            {
                comboBox_timezone.Items.Add(str);
                if (maxlength < str.Length)
                {
                    maxlength = str.Length;
                }
            }

            comboBox_timezone.SelectedItem = reg.Read("Timezone");
        }

        private void Loadweekends()
        {
            List<Weekends> listToBind = new List<Weekends>
            { new Weekends(1, "Sunday"), new Weekends(2, "Monday") , new Weekends(3, "Tuesday"), new Weekends(4, "Wednesday")  , new Weekends(5, "Thursday"), new Weekends(6, "Friday"), new Weekends(7, "Saturday")   };
            listWeekends.DisplayMember = "Text";
            listWeekends.ValueMember = "Id";
            listWeekends.DataSource = listToBind;

            atd = new Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUserHolidaysWeekends(Convert.ToInt32(reg.Read("UserId")), "0"));

            foreach (DataRow row in dt.Rows)
            {
                string curItem = row["title"].ToString();

                int index = listWeekends.FindString(curItem);
                if (index != -1)
                    listWeekends.SetSelected(index, true);
            }

        }

        private void btn_AddHoliday_Click(object sender, EventArgs e)
        {
            string title = txtTitle.Text;
            string date = monthCalendar.SelectionRange.Start.Date.ToShortDateString();
            bool everyyear = chk_yearly.Checked;
            string action = "insert";

            atd = new Attendance();
            bool result = atd.Holidays(reg.Read("UserId"), action, title, date, everyyear, 0);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string fullname = txtfullname.Text;
            string timezone = comboBox_timezone.SelectedItem.ToString();
            Hashtable ht = new Hashtable();
            String weekendDays = "";

            foreach (var item in listWeekends.SelectedItems)
            {
                weekendDays += ((Weekends)item).Text + ",";
            }

            weekendDays = weekendDays.TrimEnd(',');

            //Save Timzone
            obj.SetTimezone(timezone);

            atd = new Attendance();
            bool result = atd.UpdateUserSetting(weekendDays, Convert.ToInt32(reg.Read("UserId")), fullname);

            //Update Title in Resigtry too
            reg.Write("Title", fullname);

            //MessageBox.Show("Selected Items: " + Environment.NewLine + string.Join(Environment.NewLine, weekends));
        }

        private void lnkHolidays_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            lnkHolidays.Visible = false;
            listHoliday_panel.Visible = true;
            LoadList();
        }

        private void LoadList()
        {
            atd = new Attendance();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetUserHolidaysWeekends(Convert.ToInt32(reg.Read("UserId")), "1"));

            Holidays_grdview.DataSource = null;
            Holidays_grdview.Rows.Clear();
            Holidays_grdview.Refresh();

            //Set AutoGenerateColumns False
            Holidays_grdview.AutoGenerateColumns = false;

            //Set Columns Count
            Holidays_grdview.ColumnCount = 5;

            //Add Columns
            Holidays_grdview.Columns[0].Name = "id";
            Holidays_grdview.Columns[0].DataPropertyName = "id";
            Holidays_grdview.Columns[0].Visible = false;

            Holidays_grdview.Columns[1].Name = "title";
            Holidays_grdview.Columns[1].HeaderText = "Title";
            Holidays_grdview.Columns[1].DataPropertyName = "title";

            Holidays_grdview.Columns[2].Name = "date";
            Holidays_grdview.Columns[2].HeaderText = "Date";
            Holidays_grdview.Columns[2].DataPropertyName = "date";

            Holidays_grdview.Columns[3].Name = "everyyear";
            Holidays_grdview.Columns[3].DataPropertyName = "Yearly";
            Holidays_grdview.Columns[3].HeaderText = "Every Year";

            Holidays_grdview.Columns[4].Name = "action";
            Holidays_grdview.Columns[4].DataPropertyName = "instanceid";
            Holidays_grdview.Columns[4].HeaderText = "";

            if (dt.Rows.Count > 0)
            {
                Holidays_grdview.DataSource = dt;
                Holidays_grdview.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;


                foreach (DataGridViewRow row in Holidays_grdview.Rows)
                {
                    DataGridViewLinkCell LinkCell = new DataGridViewLinkCell();
                    LinkCell.ToolTipText = "Delete";
                    LinkCell.LinkColor = Color.Red;
                    row.Cells["action"] = LinkCell;
                    row.Cells["action"].Value = "Delete";

                }
            }
        }

        private void Holidays_grdview_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int columnIndex = Holidays_grdview.CurrentCell.ColumnIndex;
            string columnName = Holidays_grdview.Columns[columnIndex].Name;
            string columnValue = Holidays_grdview.Rows[Holidays_grdview.CurrentRow.Index].Cells[columnName].Value.ToString();
            int rowid = Convert.ToInt32(Holidays_grdview.Rows[Holidays_grdview.CurrentRow.Index].Cells["id"].Value.ToString());
            bool result = false;
            atd = new Attendance();

            if (columnName == "action")
            {
                result = atd.Holidays("0", "del", "", "", false, rowid);

                if (!result)
                {
                    MessageBox.Show("Error " + columnValue);
                }

                LoadList();
            }
        }

        private void lnkHide_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            lnkHolidays.Visible = true;
            listHoliday_panel.Visible = false;
        }
    }

    public class Weekends
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public Weekends(int id, string text)
        {
            this.Id = id;
            this.Text = text;
        }
    }
}
