﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.WebService;
using TimeAttedance.Controls;


namespace TimeAttedance
{
    public partial class AddEmployee : Form
    {
        Main obj = new Main();
        Registery reg;

        public AddEmployee()
        {
            InitializeComponent();
        }

        public AddEmployee(String view)
        {
            reg = new Registery();
            InitializeComponent();

            //obj.ReInitializeTimezone();
            //obj.CheckTimezone();

            FormLoad(view);
        }

        private void FormLoad(String view)
        {
            if (view == "Add")
            {
                btnAddUser.Visible = false;
                btnLoadUser.Visible = true;
                lblTitle.Text = "Add User";
                viewemp_panel.Visible = false;
                addemp_panel.Visible = true;
            }

            if (view == "View")
            {
                lblTitle.Text = "Users";
                lblTitle.Visible = false;
                addemp_panel.Visible = false;
                viewemp_panel.Visible = true;
                LoadEmployees(reg.Read("AppId"), true);
            }
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text == "")
            {
                MessageBox.Show("Please enter Email Id", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmail.Focus();
                return;
            }
            if (txtFname.Text == "")
            {
                MessageBox.Show("Please enter Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFname.Focus();
                return;
            }

            if (obj.ValidateEmail(txtEmail.Text))
            {
                string email = txtEmail.Text;
                string fname = txtFname.Text;
                string lname = txtLname.Text;

                string Message = obj.AddUser(fname, lname, email);
                //Attendance atd = new Attendance();
                //string Message = atd.AddEmployee(fname, lname, email, reg.Read("UserId"), reg.Read("AppId"), reg.Read("InstanceId"));

                //test
                //string Message = "Employee added successfully";

                MessageBox.Show(Message, "Information", MessageBoxButtons.OK);

                if (Message.Contains("successfully"))
                {
                    //this.Close();
                    //AddEmployee frm = new AddEmployee("View");
                    //frm.Size = new Size(560, 400);
                    //frm.Show();
                    FormLoad("View");
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please provide valid Email Address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmail.Focus();
            }
        }

        private void grd_employees_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int columnIndex = grd_employees.CurrentCell.ColumnIndex;
            string columnName = grd_employees.Columns[columnIndex].Name;
            string columnValue = grd_employees.Rows[grd_employees.CurrentRow.Index].Cells[columnName].Value.ToString();

            string username = grd_employees.Rows[grd_employees.CurrentRow.Index].Cells["title"].Value.ToString();

            if (columnName == "access_given" || columnName == "access_taken")
            {
                if (columnValue == "False")
                {
                    string Msg = (columnName == "access_given") ? "Are you sure to give access to \"" + username + "\"?" : "Do you want to request \"" + username + "\" for access?";

                    DialogResult dialog = MessageBox.Show(Msg, "Information", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        DataGridViewTextBoxCell TextBoxCell = new DataGridViewTextBoxCell();
                        TextBoxCell.Style.ForeColor = Color.Red;
                        TextBoxCell.ToolTipText = "Request for view/modify clockin/clockout status is pending";
                        grd_employees.Rows[grd_employees.CurrentRow.Index].Cells[columnName] = TextBoxCell;
                        grd_employees.Rows[grd_employees.CurrentRow.Index].Cells[columnName].Value = "Request Pending..";

                        grd_employees.Refresh();
                        grd_employees.Update();

                        int userid = Convert.ToInt32(grd_employees.Rows[e.RowIndex].Cells["id"].Value);

                        bool result = RequestAccess(grd_employees.Rows[e.RowIndex].Cells, columnName);
                        //if (result)
                        //    MessageBox.Show("Request successfully sent!");
                        //else
                        //    MessageBox.Show("Request could not be send. Sorry for the inconvinience!");
                    }
                }
                else if (columnValue == "True")
                {
                    string Msg = (columnName == "access_given") ? "Are you sure to revoke access of \"" + username + "\"?" : "Do you really want to revoke \"" + username + "\"  access?";

                    DialogResult dialog = MessageBox.Show(Msg, "Information", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        DataGridViewCheckBoxCell CheckBoxCell = new DataGridViewCheckBoxCell();
                        CheckBoxCell.ToolTipText = "Request for access to view/modify clockin/clockout status";
                        grd_employees.Rows[grd_employees.CurrentRow.Index].Cells[columnName] = CheckBoxCell;
                        grd_employees.Rows[grd_employees.CurrentRow.Index].Cells[columnName].Value = false;

                        grd_employees.Refresh();
                        grd_employees.Update();

                        bool result = RevokeAccess(grd_employees.Rows[e.RowIndex].Cells, columnName);
                        //if (result)
                        //    MessageBox.Show("Request successfully sent!");
                        //else
                        //    MessageBox.Show("Request could not be send. Sorry for the inconvinience!");
                    }
                }
            }
            //else
            //{
            //    string rowid = grd_employees.Rows[e.RowIndex].Cells["accessid"].Value.ToString();
            //    string empid = grd_employees.Rows[e.RowIndex].Cells["id"].Value.ToString();
            //    AdminRights adm = new AdminRights(rowid, empid);
            //    this.Close();
            //    adm.Show();
            //}



            //string cellType1 = grd_employees.Columns[e.ColumnIndex].CellType.Name;
            //Type cellType = grd_employees.Rows[grd_employees.CurrentRow.Index].Cells[columnName].ValueType;
            //if (cellType.Name == "Boolean")
            //{
            //    DialogResult dialog = MessageBox.Show("Do you want to request for access?", "Information", MessageBoxButtons.YesNo);

            //    if (dialog == DialogResult.Yes)
            //    {
            //        DataGridViewTextBoxCell TextBoxCell = new DataGridViewTextBoxCell();
            //        TextBoxCell.Style.ForeColor = Color.Red;
            //        TextBoxCell.ToolTipText = "Request for view/modify clockin/clockout status is pending";
            //        grd_employees.Rows[grd_employees.CurrentRow.Index].Cells["access"] = TextBoxCell;
            //        grd_employees.Rows[grd_employees.CurrentRow.Index].Cells["access"].Value = "Request Pending..";

            //        grd_employees.Refresh();
            //        grd_employees.Update();

            //        int userid = Convert.ToInt32(grd_employees.Rows[e.RowIndex].Cells["id"].Value);

            //        bool result = RequestAccess(grd_employees.Rows[e.RowIndex].Cells);
            //        if (result)
            //            MessageBox.Show("Request successfully sent!");
            //        else
            //            MessageBox.Show("Request could not be send. Sorry for the inconvinience!");
            //    }
            //}
            //else
            //{
            //    string rowid = grd_employees.Rows[e.RowIndex].Cells["adminright"].Value.ToString();
            //    string empid = grd_employees.Rows[e.RowIndex].Cells["id"].Value.ToString();
            //    AdminRights adm = new AdminRights(rowid,empid);
            //    this.Close();
            //    adm.Show();
            //}


            //check if column exists
            if ((columnName == "access_given" || columnName == "access_taken") && e.RowIndex >= 0)
                this.grd_employees.CommitEdit(DataGridViewDataErrorContexts.Commit);

        }

        public void LoadEmployees(string AppId, bool Active)
        {
            AvaimaTime.AvaimaTimeZoneAPI atp = new AvaimaTime.AvaimaTimeZoneAPI();
            Attendance atd = new Attendance();

            //Active Employees
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(atd.GetEmployees("0", reg.Read("InstanceId"), Active, Convert.ToInt32(reg.Read("UserId"))));

            grd_employees.DataSource = null;
            grd_employees.Rows.Clear();
            grd_employees.AutoGenerateColumns = false;
            grd_employees.ColumnCount = 4;

            grd_employees.Columns[0].Name = "title";
            grd_employees.Columns[0].HeaderText = "Title";
            grd_employees.Columns[0].DataPropertyName = "Title";

            grd_employees.Columns[1].Name = "email";
            grd_employees.Columns[1].HeaderText = "Email";
            grd_employees.Columns[1].DataPropertyName = "email";

           

            //grd_employees.Columns[2].Name = "access_taken";
            //grd_employees.Columns[2].HeaderText = "You can access statistics";
            //grd_employees.Columns[2].DataPropertyName = "access_taken";

            //grd_employees.Columns[3].Name = "access_given";
            //grd_employees.Columns[3].HeaderText = "Can access your statistics";
            //grd_employees.Columns[3].DataPropertyName = "access_given";

            grd_employees.Columns[2].Name = "id";
            grd_employees.Columns[2].DataPropertyName = "ID";
            grd_employees.Columns[2].Visible = false;

            grd_employees.Columns[3].Name = "accessid";
            grd_employees.Columns[3].DataPropertyName = "accessid";
            grd_employees.Columns[3].Visible = false;
            
            ////////////////

            DataGridViewCheckBoxColumn access_given_column = new DataGridViewCheckBoxColumn();
            access_given_column.Name = "access_given";
            access_given_column.HeaderText = "Can access your statistics";
            access_given_column.DataPropertyName = "access_given";
            grd_employees.Columns.Add(access_given_column);

            DataGridViewCheckBoxColumn access_taken_column = new DataGridViewCheckBoxColumn();
            access_taken_column.Name = "access_taken";
            access_taken_column.HeaderText = "You can access statistics";
            access_taken_column.DataPropertyName = "access_taken";
            grd_employees.Columns.Add(access_taken_column);

            ////////////////

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            style.BackColor = Color.LightGray;
           
            //Hide Request columns for inactive user
            if (!Active)
            {
                grd_employees.Columns["access_given"].Visible = false;
                grd_employees.Columns["access_taken"].Visible = false;
            }
            else
            {
                grd_employees.Columns["access_given"].Visible = true;
                grd_employees.Columns["access_taken"].Visible = true;
            }


            var height = 200;
            var width = 950;
            int i = 0;

            if (dt.Rows.Count > 0)
            {
                grd_employees.DataSource = dt;
                grd_employees.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                foreach (DataGridViewRow row in grd_employees.Rows)
                {
                    //string DateTime = row.Cells["lastinDate"].Value.ToString();

                    //if (!string.IsNullOrEmpty(row.Cells["lastinDate"].Value.ToString()))
                    //{
                    //    row.Cells["lastinDate"].Value = atp.GetDate(Convert.ToDateTime(DateTime).ToString(), AppId);
                    //    //row.Cells["lastinTime"].Value = atp.GetTime(obj.FormatDate(DateTime).ToShortTimeString(), AppId);
                    //}

                    //if ((!string.IsNullOrEmpty(dt.Rows[i]["lastout"].ToString())) || (string.IsNullOrEmpty(dt.Rows[i]["lastout"].ToString()) && string.IsNullOrEmpty(dt.Rows[i]["lastin"].ToString())))
                    //{
                    //    row.Cells["status"].Value = "";
                    //}
                    //else if (string.IsNullOrEmpty(dt.Rows[i]["lastout"].ToString()) && (!string.IsNullOrEmpty(dt.Rows[i]["lastin"].ToString())))
                    //{
                    //    row.Cells["status"].Value = "Present";
                    //}

                    //Access 
                    if (Active)
                    {
                        DataGridViewCheckBoxCell CheckBoxCell;
                        DataGridViewTextBoxCell TextBoxCell;
                        //CheckBoxCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

                        if (!string.IsNullOrEmpty(row.Cells["access_given"].Value.ToString()) && row.Cells["access_given"].Value.ToString() != "False")
                        {
                            CheckBoxCell = new DataGridViewCheckBoxCell();
                            CheckBoxCell.ToolTipText = "Access given to view/modify your clockin/clockout status";
                            row.Cells["access_given"] = CheckBoxCell;
                            row.Cells["access_given"].Value = true;
                        }
                        else
                        {
                            string permit = (string.IsNullOrEmpty(dt.Rows[i]["permission_given"].ToString())) ? "" : dt.Rows[i]["permission_given"].ToString();
                            if (permit == "pending")
                            {
                                //TextBoxCell = new DataGridViewTextBoxCell();
                                //TextBoxCell.ToolTipText = "Request for access to clockin/clockout status is pending";
                                //TextBoxCell.Style.ForeColor = Color.Red;
                                //row.Cells["access_given"] = TextBoxCell;
                                //row.Cells["access_given"].Value = "Request Pending..";                                

                                row.Cells["access_given"].Value = null;
                                row.Cells["access_given"].ToolTipText = "Request Pending..";                               
                                row.Cells["access_given"].Style = style;
                                style.ApplyStyle(style);
                            }
                            else
                            {

                                CheckBoxCell = new DataGridViewCheckBoxCell();
                                CheckBoxCell.ToolTipText = "Request for access to view/modify clockin/clockout status";
                                row.Cells["access_given"] = CheckBoxCell;
                                row.Cells["access_given"].Value = false;
                            }
                        }


                        if (!string.IsNullOrEmpty(row.Cells["access_taken"].Value.ToString()) && row.Cells["access_taken"].Value.ToString() != "False")
                        {
                            CheckBoxCell = new DataGridViewCheckBoxCell();
                            CheckBoxCell.ToolTipText = "You can view/modify clockin/clockout status";
                            row.Cells["access_taken"] = CheckBoxCell;
                            row.Cells["access_taken"].Value = true;
                        }
                        else
                        {
                            string permit = (string.IsNullOrEmpty(dt.Rows[i]["permission_taken"].ToString())) ? "" : dt.Rows[i]["permission_taken"].ToString();
                            if (permit == "pending")
                            {
                                //TextBoxCell = new DataGridViewTextBoxCell();
                                //TextBoxCell.ToolTipText = "Request for access to clockin/clockout status is pending";
                                //TextBoxCell.Style.ForeColor = Color.Red;
                                //row.Cells["access_taken"] = TextBoxCell;
                                //row.Cells["access_taken"].Value = "Request Pending..";

                                row.Cells["access_taken"].Value = null;
                                row.Cells["access_taken"].ToolTipText = "Request Pending..";
                                row.Cells["access_taken"].Style = style;
                                style.ApplyStyle(style);
                            }
                            else
                            {
                                CheckBoxCell = new DataGridViewCheckBoxCell();
                                CheckBoxCell.ToolTipText = "Request for access to view/modify clockin/clockout status";
                                row.Cells["access_taken"] = CheckBoxCell;
                                row.Cells["access_taken"].Value = false;
                            }
                        }



                        //if (string.IsNullOrEmpty(row.Cells["adminright"].Value.ToString()))
                        //{
                        //    row.Cells["access"] = (DataGridViewCheckBoxCell)Cell("checkbox");
                        //    DataGridViewCheckBoxCell CheckBoxCell = new DataGridViewCheckBoxCell();
                        //    CheckBoxCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                        //    CheckBoxCell.ToolTipText = "Request for access to view/modify clockin/clockout status";
                        //    row.Cells["access"] = CheckBoxCell;
                        //    row.Cells["access"].Value = false;
                        //}
                        //else
                        //{
                        //    if (dt.Rows[i]["permission"].ToString() == "allow")
                        //    {
                        //        DataGridViewLinkCell LinkCell = new DataGridViewLinkCell();
                        //        LinkCell.ToolTipText = "View/modify clockin/clockout status";
                        //        row.Cells["access"] = LinkCell;
                        //        row.Cells["access"].Value = "View Statistics";

                        //    }
                        //    else
                        //    {
                        //        DataGridViewTextBoxCell TextBoxCell = new DataGridViewTextBoxCell();
                        //        TextBoxCell.Style.ForeColor = Color.Red;
                        //        TextBoxCell.ToolTipText = "Request for access to view/modify clockin/clockout status is pending";
                        //        row.Cells["access"] = TextBoxCell;
                        //        row.Cells["access"].Value = "Request Pending..";
                        //    }
                        //}

                    }
                    else
                    {
                        grd_employees.Columns["accessid"].Visible = false;
                    }

                    if (row.Visible)
                        height += row.Height;

                    i++;
                }

                //Form dynamic size
                foreach (DataGridViewColumn col in grd_employees.Columns)
                {
                    if (col.Visible)
                        width += col.Width;
                }

                viewemp_panel.Height = grd_employees.Height = height;
                viewemp_panel.Width = grd_employees.Width = width;

            }
            else
            {
                grd_employees.Rows.Add(1);
                grd_employees[0, 0].Value = "";
                grd_employees[1, 0].Value = "No entry";
            }

            SetMenu(Active);
        }

        protected override void OnLoad(EventArgs e)
        {
            LoadEmployees(reg.Read("AppId"), true);  //<- does correctly render controls
            base.OnLoad(e);
        }
        private void AddEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                obj.NotifyIcon(sender);
                this.Hide();
                e.Cancel = true;
            }
        }

        private void inactiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menu = (ToolStripMenuItem)sender;

            if (menu.Text == "View Inactive Users")
                LoadEmployees(reg.Read("AppId"), false);
            else
                LoadEmployees(reg.Read("AppId"), true);
        }

        public object Cell(string type)
        {
            if (type == "checkbox")
            {
                DataGridViewCheckBoxCell cell1 = new DataGridViewCheckBoxCell()
                {
                    TrueValue = true,
                    FalseValue = false,
                    ToolTipText = "Request to grant access to view/modify clockin/clockout status",
                };
                cell1.Value = false;

                return cell1;
            }

            else if (type == "link")
            {
                DataGridViewLinkCell cell2 = new DataGridViewLinkCell()
                {
                    ToolTipText = "View/modify clockin/clockout status",
                };
                cell2.Value = "View Statistics";

                return cell2;
            }

            return new object();
        }

        public void SetMenu(bool active)
        {
            if (active)
            {
                subMenu.Items[0].Text = "View Inactive Users";
            }
            else
            {
                subMenu.Items[0].Text = "View Active Users";
            }
        }

        public bool RequestAccess(DataGridViewCellCollection cell, string type)
        {
            Attendance atd = new Attendance();
            bool result;
            string permission;

            int userid = Convert.ToInt32(cell["id"].Value);

            int accessTo = Convert.ToInt32(reg.Read("UserId"));
            string Title = reg.Read("Title");
            string instanceid = reg.Read("InstanceId");

            permission = "pending";
            if (type == "access_given")
                result = atd.RequestAdminRights_Email(userid, accessTo, Title, "allow", instanceid, false, true);
            else
                result = atd.RequestAdminRights_Email(userid, accessTo, Title, "pending", instanceid, true, false);

            //result = true;
            if (result)
                return true;
            else
                return false;
        }

        public bool RevokeAccess(DataGridViewCellCollection cell, string type)
        {
            Attendance atd = new Attendance();
            bool result = false;
            int userid;
            int accessTo;

            string instanceid = reg.Read("InstanceId");
            string Title = reg.Read("Title");

            if (type == "access_given")
            {
                userid = Convert.ToInt32(reg.Read("UserId"));
                accessTo = Convert.ToInt32(cell["id"].Value);
                result = atd.RevokeAdminRights_Email(userid, accessTo, Title, type, instanceid, false, true);
            }
            else if (type == "access_taken")
            {
                userid = Convert.ToInt32(cell["id"].Value);
                accessTo = Convert.ToInt32(reg.Read("UserId"));
                result = atd.RevokeAdminRights_Email(userid, accessTo, Title, type, instanceid, true, false);
            }

            //  result = true;
            if (result)
                return true;
            else
                return false;
        }

        private void adminRightaccessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ParentForm.Hide();
            MyApplication HomePage = new MyApplication("adminrights");
            HomePage.Show();
            
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            btnAddUser.Visible = false;
            btnLoadUser.Visible = true;
            FormLoad("Add");
        }

        private void btnLoadUser_Click(object sender, EventArgs e)
        {
            btnAddUser.Visible = true;
            btnLoadUser.Visible = false;
            FormLoad("View");
        }


    }
}
