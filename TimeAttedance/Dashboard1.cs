﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TimeAttedance
{
    public partial class Dashboard1 : Form
    {
        Stopwatch stopWatch = new Stopwatch();

        public Dashboard1()
        {
            InitializeComponent();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            MyFunction();

        }

        public void MyFunction()
        {
            lblTime.Text = "RunTime 00:00:00";

        }

        private void Start_Click(object sender, EventArgs e)
        {
            stopWatch.Start();
            timer1.Start();
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            stopWatch.Stop();
            stopWatch.Reset();
          //  lblTime.Text = "RunTime 00:00:00";

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            TimeSpan ts = stopWatch.Elapsed;

            // Format and display the TimeSpan value. 
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            lblTime.Text = elapsedTime;
        }
    }
}
