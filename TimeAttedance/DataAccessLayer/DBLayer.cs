using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using TimeAttedance.DataAccessLayer;

namespace TimeAttedance
{
    public class DBLayer
    {
        public DBLayer()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        static public SqlConnection GetConnection()
        {
            SqlConnection Cn = null;
            try
            {

                Registery reg = new Registery();
                string connectionString = reg.connectionString;
                Cn = new SqlConnection(connectionString);
                Cn.Open();

            }
            catch (Exception e)
            {
                throw e;
            }
            return Cn;
        }
        public static int ExecuteSQL(string SQL)
        {
            int RetVal = -1;
            SqlConnection Cn = GetConnection();
            try
            {
                if (Cn != null)
                {
                    SqlCommand Cmd = new SqlCommand(SQL, Cn);
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Cn.Close();
                Cn.Dispose();
            }
            return RetVal;
        }
        public static int ExecuteSQL(string SQL, SqlConnection Cn)
        {
            int RetVal = -1;
            try
            {
                if (Cn != null)
                {
                    SqlCommand Cmd = new SqlCommand(SQL, Cn);
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return RetVal;
        }
        public static SqlCommand GetCommand(string SQL)
        {
            SqlCommand Cmd = null;
            SqlConnection Cn = GetConnection();
            try
            {
                if (Cn != null)
                {
                    Cmd = new SqlCommand(SQL, Cn);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                //Cn.Close();
                //Cn.Dispose();
            }
            return Cmd;
        }
        public static SqlCommand GetCommand(string SQL, ref SqlConnection Cn)
        {
            SqlCommand Cmd = null;
            try
            {
                if (Cn != null)
                {
                    Cmd = new SqlCommand(SQL, Cn);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return Cmd;
        }
        public static int ExecuteCommand(SqlCommand Cmd)
        {
            int RetVal = -1;
            try
            {
                if (Cmd.Connection != null)
                {
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Cmd.Connection.Close();
                Cmd.Connection.Dispose();
            }
            return RetVal;
        }
        public static int ExecuteCommandNoClose(SqlCommand Cmd)
        {
            int RetVal = -1;
            try
            {
                if (Cmd.Connection != null)
                {
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return RetVal;
        }
        public static DataSet GetDataSet(string SQL)
        {
            SqlCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                Cmd = GetCommand(SQL);
                SqlDataAdapter Adp = new SqlDataAdapter(Cmd);
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cmd != null)
                {
                    if (Cmd.Connection != null)
                    {
                        Cmd.Connection.Close();
                        Cmd.Connection.Dispose();
                    }
                }
            }
            return ds;
        }
        public static DataSet GetDataSet(string SQL, SqlConnection Cn)
        {
            SqlCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                if (Cn != null)
                {
                    Cmd = new SqlCommand(SQL, Cn);
                }
                SqlDataAdapter Adp = new SqlDataAdapter(Cmd);
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return ds;
        }
        public static object ExecuteScalar(string SQL, SqlConnection Cn)
        {
            object RetVal = -1;
            try
            {
                if (Cn != null)
                {
                    SqlCommand Cmd = new SqlCommand(SQL, Cn);
                    RetVal = Cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return RetVal;
        }
        public static object ExecuteScalar(string SQL)
        {
            object RetVal = -1;
            SqlConnection Cn = GetConnection();
            try
            {
                if (Cn != null)
                {
                    SqlCommand Cmd = new SqlCommand(SQL, Cn);
                    RetVal = Cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cn != null)
                {
                    Cn.Close();
                    Cn.Dispose();
                }
            }
            return RetVal;
        }
        public static object ExecuteScalar(SqlCommand Cmd)
        {
            object RetVal = null;
            try
            {
                if (Cmd.Connection != null)
                {
                    RetVal = Cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cmd.Connection != null)
                {
                    Cmd.Connection.Close();
                    Cmd.Connection.Dispose();
                }
            }
            return RetVal;
        }
        static public SqlDataReader ExecuteReader(string SQL)
        {
            SqlConnection Cn = GetConnection();
            SqlCommand Cmd = GetCommand(SQL, ref Cn);
            return Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
        static public SqlDataReader ExecuteReader(string SQL, SqlConnection Cn)
        {
            SqlCommand Cmd = GetCommand(SQL, ref Cn);
            return Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
        public static DataSet GetDataSet(string SQL, SqlParameterCollection SPC)
        {
            SqlCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                Cmd = GetCommand(SQL);
                foreach (SqlParameter Sp in SPC)
                {
                    Cmd.Parameters.Add(new SqlParameter(Sp.ParameterName, Sp.Value));
                }
                SqlDataAdapter Adp = new SqlDataAdapter(Cmd);
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cmd != null)
                {
                    if (Cmd.Connection != null)
                    {
                        Cmd.Connection.Close();
                        Cmd.Connection.Dispose();
                    }
                }
            }
            return ds;
        }
        public static SqlParameterCollection GetParameterCollection()
        {
            SqlCommand Cmd = new SqlCommand();
            return Cmd.Parameters;
        }




    }
}
