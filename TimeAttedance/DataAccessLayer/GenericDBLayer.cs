using System;
using System.Data;
//using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Data.Common;


namespace TimeAttedance
{
    public class GenericDBLayer
    {
        public GenericDBLayer()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        static private DbProviderFactory GetFactory()
        {
            string provider = ConfigurationManager.ConnectionStrings["ConStr"].ProviderName;
            DbProviderFactory dbFactory = DbProviderFactories.GetFactory(provider);
            return dbFactory;
        }
        static public DbConnection GetConnection()
        {
            DbConnection Cn = null;
            try
            {
                //string provider = ConfigurationManager.AppSettings["DbProvider"];

                Cn = GetFactory().CreateConnection();
                Cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConStr"].ConnectionString;
                Cn.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
            return Cn;
        }
        public static int ExecuteSQL(string SQL)
        {
            int RetVal = -1;
            DbConnection Cn = GetConnection();
            try
            {
                RetVal = ExecuteSQL(SQL, Cn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Cn.Close();
                Cn.Dispose();
            }
            return RetVal;
        }
        public static int ExecuteSQL(string SQL, DbConnection Cn)
        {
            int RetVal = -1;
            DbCommand Cmd = null;
            try
            {
                if (Cn != null)
                {
                    Cmd = GetCommand(SQL);
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                
            }
            return RetVal;
        }

        public static int ExecuteSQL(string SQL, DbTransaction trans)
        {
            int RetVal = -1;
            DbCommand Cmd = null;
            try
            {
                if (trans != null)
                {
                    Cmd = GetCommand(SQL, trans);
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                
            }
            return RetVal;
        }

        public static DbCommand GetCommand(string SQL)
        {
            DbCommand Cmd = null;
            DbConnection Cn = GetConnection();
            try
            {
                if (Cn != null)
                {
                    GetCommand(SQL, ref Cn);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                //Cn.Close();
                //Cn.Dispose();
            }
            return Cmd;
        }

        public static DbCommand GetCommand(string SQL, ref DbConnection Cn)
        {
            DbCommand Cmd = null;
            try
            {
                if (Cn != null)
                {
                    Cmd = Cn.CreateCommand();
                    Cmd.CommandText = SQL;
                    Cmd.CommandType = CommandType.Text;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return Cmd;
        }

        public static DbCommand GetCommand(string SQL, DbTransaction trans)
        {
            DbCommand Cmd = null;
            try
            {
                if (trans != null)
                {
                    Cmd = trans.Connection.CreateCommand();
                    Cmd.CommandText = SQL;
                    Cmd.Transaction = trans;
                    Cmd.CommandType = CommandType.Text;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return Cmd;
        }

        public static int ExecuteCommand(DbCommand Cmd)
        {
            int RetVal = -1;
            try
            {
                if (Cmd.Connection != null)
                {
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                Cmd.Connection.Close();
                Cmd.Connection.Dispose();
            }
            return RetVal;
        }
        public static int ExecuteCommandNoClose(DbCommand Cmd)
        {
            int RetVal = -1;
            try
            {
                if (Cmd.Connection != null)
                {
                    RetVal = Cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return RetVal;
        }
        public static DataSet GetDataSet(string SQL)
        {
            DbConnection Cn = null;
            DataSet ds = new DataSet();
            try
            {
                Cn = GetConnection();
                ds = GetDataSet(SQL, Cn);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cn != null)
                {
                    Cn.Close();
                    Cn.Dispose();
                }
            }
            return ds;
        }
        public static DataSet GetDataSet(string SQL, DbConnection Cn)
        {
            DbCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                if (Cn != null)
                {
                    Cmd = GetCommand(SQL, ref Cn);
                }
                DbDataAdapter Adp = GetFactory().CreateDataAdapter();
                Adp.SelectCommand = Cmd;
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return ds;
        }

        public static DataSet GetDataSet(string SQL, DbTransaction trans)
        {
            DbCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                if (trans != null)
                {
                    Cmd = GetCommand(SQL, trans);
               }
                DbDataAdapter Adp = GetFactory().CreateDataAdapter();
                Adp.SelectCommand = Cmd;
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
            return ds;
        }

        public static object ExecuteScalar(string SQL, DbConnection Cn)
        {
            object RetVal = -1;
            try
            {
                if (Cn != null)
                {
                    DbCommand Cmd = GetCommand(SQL, ref Cn);
                    RetVal = Cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return RetVal;
        }

        public static object ExecuteScalar(string SQL, DbTransaction trans)
        {
            object RetVal = -1;
            try
            {
                if (trans != null)
                {
                    DbCommand Cmd = GetCommand(SQL, trans);
                    RetVal = Cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
            }
            return RetVal;
        }

        public static object ExecuteScalar(string SQL)
        {
            object RetVal = -1;
            DbConnection Cn = GetConnection();
            try
            {
                if (Cn != null)
                {
                    RetVal = ExecuteScalar(SQL, Cn);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cn != null)
                {
                    Cn.Close();
                    Cn.Dispose();
                }
            }
            return RetVal;
        }
        public static object ExecuteScalar(DbCommand Cmd)
        {
            object RetVal = null;
            try
            {
                if (Cmd.Connection != null)
                {
                    RetVal = Cmd.ExecuteScalar();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cmd.Connection != null)
                {
                    Cmd.Connection.Close();
                    Cmd.Connection.Dispose();
                }
            }
            return RetVal;
        }
        static public DbDataReader ExecuteReader(string SQL)
        {
            DbConnection Cn = null;
            try
            {
                Cn = GetConnection();
                return ExecuteReader(SQL, Cn);
            }
            catch (Exception e)
            { throw e; }
            finally
            {

            }
            return null;
        }
        static public DbDataReader ExecuteReader(string SQL, DbConnection Cn)
        {
            try
            {
                DbCommand Cmd = GetCommand(SQL, ref Cn);
                return Cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
        }

        static public DbDataReader ExecuteReader(string SQL, DbTransaction trans)
        {
            try
            {
                DbCommand Cmd = GetCommand(SQL);
                Cmd.Transaction = trans;
                return Cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {

            }
        }

        public static DataSet GetDataSet(string SQL, DbParameterCollection SPC)
        {
            DbCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                Cmd = GetCommand(SQL);
                foreach (DbParameter Sp in SPC)
                {
                    DbParameter dbParam = GetFactory().CreateParameter();
                    dbParam.ParameterName = Sp.ParameterName;
                    dbParam.Value = Sp.Value;
                    Cmd.Parameters.Add(dbParam);
                }

                DbDataAdapter Adp = GetFactory().CreateDataAdapter();
                Adp.SelectCommand = Cmd;
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cmd != null)
                {
                    if (Cmd.Connection != null)
                    {
                        Cmd.Connection.Close();
                        Cmd.Connection.Dispose();
                    }
                }
            }
            return ds;
        }

        public static DataSet GetDataSet(string SQL, DbParameterCollection SPC, DbTransaction trans)
        {
            DbCommand Cmd = null;
            DataSet ds = new DataSet();
            try
            {
                Cmd = GetCommand(SQL, trans);
                foreach (DbParameter Sp in SPC)
                {
                    DbParameter dbParam = GetFactory().CreateParameter();
                    dbParam.ParameterName = Sp.ParameterName;
                    dbParam.Value = Sp.Value;
                    Cmd.Parameters.Add(dbParam);
                }
                DbDataAdapter Adp = GetFactory().CreateDataAdapter();
                Adp.SelectCommand = Cmd;
                Adp.Fill(ds);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (Cmd != null)
                {
                    if (Cmd.Connection != null)
                    {
                        Cmd.Connection.Close();
                        Cmd.Connection.Dispose();
                    }
                }
            }
            return ds;
        }

        public static DbParameterCollection GetParameterCollection()
        {

            DbCommand Cmd = GetFactory().CreateCommand();
            return Cmd.Parameters;
        }

        public static DbTransaction GetTransaction()
        {
            return GetConnection().BeginTransaction();
        }




    }
}