using System;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data.SqlClient;
/// <summary>
/// This is a generic class to execute all types of stored procedures  
/// </summary>
/// 
namespace TimeAttedance
{
    public class StoredProcedures : DBLayer
    {
        public StoredProcedures()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        //-------------------------------Stored Procedure Functionality-------------------------------//
        #region Stored Procedures
        //**********************To Fetch DataSet from stored Procedure****************************//
        /// <summary>
        /// To select entire columns from database then you may use this function
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC"></param>
        /// <returns></returns>
        public static DataSet GetDataSetStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC)
        {
            SqlCommand sqlcom = null;
            DataSet ds = new DataSet();
            try
            {
                SqlConnection sqlcon = GetConnection();
                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter SP in SPC)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP.ParameterName, SP.Value));
                    }
                }
                SqlDataAdapter ADP = new SqlDataAdapter(sqlcom);
                ADP.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return ds;
        }
        //**********************To Fetch DataSet from stored Procedure with different Connection****************************//
        /// <summary>
        /// To select entire columns from database then you may use this function
        /// For Different SqlConnection
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        public static DataSet GetDataSetStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC, SqlConnection sqlcon)
        {
            SqlCommand sqlcom = null;
            DataSet ds = new DataSet();
            try
            {
                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter SP in SPC)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP.ParameterName, SP.Value));
                    }
                }
                SqlDataAdapter ADP = new SqlDataAdapter(sqlcom);
                ADP.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return ds;
        }
        //**********************To Execute  any stored Procedure for DML****************************//
        /// <summary>
        /// To Execute those stored procedures in which there is no selection of records then you may use this Function
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC"></param>
        /// <returns></returns>
        public static bool ExceuteStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC)
        {
            SqlCommand sqlcom = null;
            bool RetValue = false;
            try
            {
                SqlConnection sqlcon = GetConnection();
                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter SP in SPC)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP.ParameterName, SP.Value));
                    }
                }
                sqlcom.ExecuteNonQuery();
                RetValue = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return RetValue;
        }
        //**********************To Execute  any stored Procedure for DML with different Connection****************************//
        /// <summary>
        /// To Execute those stored procedures in which there is no selection of records then you may use this Function
        /// with different SqlConnection
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        public static bool ExceuteStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC, SqlConnection sqlcon)
        {
            SqlCommand sqlcom = null;
            bool RetValue = false;
            try
            {

                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter SP in SPC)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP.ParameterName, SP.Value));
                    }
                }
                sqlcom.ExecuteNonQuery();
                RetValue = true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return RetValue;
        }
        //**********************To fetch the return value of any stored Procedure****************************//
        /// <summary>
        /// To get Return Value of a stored Procedure you can use this function
        /// in SqlParameterCollection of Output Parameter. 
        /// Must have Parameter Name and Value for SqlParameterCollection of Input Parameter
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC"></param>
        /// <returns></returns>
        public static string GetReturnVariableOfStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC)
        {
            SqlCommand sqlcom = null;
            string RetValue = string.Empty;

            try
            {
                SqlConnection sqlcon = GetConnection();
                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    SqlParameter sqlParam = sqlcom.Parameters.Add("@ReturnValue", SqlDbType.Int);
                    sqlParam.Direction = ParameterDirection.ReturnValue;
                    foreach (SqlParameter SP in SPC)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP.ParameterName, SP.Value));
                    }

                }
                sqlcom.ExecuteNonQuery();
                RetValue = sqlcom.Parameters["@ReturnValue"].Value.ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return RetValue;
        }
        //**********************To fetch the return value of any stored Procedure  with different connection****************************//
        /// <summary>
        /// To get Return Value of a stored Procedure you can use this function
        /// in SqlParameterCollection of Output Parameter. 
        /// Must have Parameter Name and Value for SqlParameterCollection of Input Parameter
        /// For Different SqlConnection
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        public static string GetReturnVariableOfStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC, SqlConnection sqlcon)
        {
            SqlCommand sqlcom = null;
            string RetValue = string.Empty;
            try
            {

                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;
                    SqlParameter sqlParam = sqlcom.Parameters.Add("@ReturnValue", SqlDbType.Int);
                    sqlParam.Direction = ParameterDirection.ReturnValue;
                    foreach (SqlParameter SP in SPC)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP.ParameterName, SP.Value));
                    }

                }
                sqlcom.ExecuteNonQuery();
                RetValue = sqlcom.Parameters["@ReturnValue"].Value.ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return RetValue;
        }
        //**********************To Fetch all output Variables of Stored procedures**************************************//
        /// <summary>
        /// To get the output parameter collection for Stored Procedure you must have parameter name and and parameter SqlDBType
        /// in SqlParameterCollection of Output Parameter. 
        /// Must have Parameter Name and Value for SqlParameterCollection of Input Parameter
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC_Input"></param>
        /// <param name="SPC_Output"></param>
        /// <returns></returns>
        public static ArrayList GetOutputVariablesOfStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC_Input, SqlParameterCollection SPC_Output)
        {
            SqlCommand sqlcom = null;
            ArrayList OutputParametersValues = new ArrayList();
            SqlParameter OutPutParams = null;
            try
            {
                SqlConnection sqlcon = GetConnection();
                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter SP_Input in SPC_Input)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP_Input.ParameterName, SP_Input.Value));
                    }
                    foreach (SqlParameter SP_Output in SPC_Output)
                    {
                        OutPutParams = new SqlParameter(SP_Output.ParameterName, SP_Output.SqlDbType);
                        OutPutParams.Direction = ParameterDirection.Output;
                        sqlcom.Parameters.Add(OutPutParams);
                    }

                }
                sqlcom.ExecuteNonQuery();


                foreach (SqlParameter SP_Output in SPC_Output)
                {
                    OutputParametersValues.Add(sqlcom.Parameters[SP_Output.ParameterName.ToString()].Value.ToString());
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return OutputParametersValues;
        }
        //**********************To Fetch all output Variables of Stored procedures using Different Connection**************************************//
        /// <summary>
        /// To get the output parameter collection for Stored Procedure you must have parameter name and and parameter SqlDBType
        /// in SqlParameterCollection of Output Parameter. 
        /// Must have Parameter Name and Value for SqlParameterCollection of Input Parameter
        /// For Different SqlConnection
        /// </summary>
        /// <param name="StoredProcedureName"></param>
        /// <param name="SPC_Input"></param>
        /// <param name="SPC_Output"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        public static ArrayList GetOutputVariablesOfStoredProcedure(string StoredProcedureName, SqlParameterCollection SPC_Input, SqlParameterCollection SPC_Output, SqlConnection sqlcon)
        {
            SqlCommand sqlcom = null;
            ArrayList OutputParametersValues = new ArrayList();
            SqlParameter OutPutParams = null;
            try
            {

                if (sqlcon != null)
                {
                    sqlcom = new SqlCommand(StoredProcedureName, sqlcon);
                    sqlcom.CommandType = CommandType.StoredProcedure;

                    foreach (SqlParameter SP_Input in SPC_Input)
                    {
                        sqlcom.Parameters.Add(new SqlParameter(SP_Input.ParameterName, SP_Input.Value));
                    }
                    foreach (SqlParameter SP_Output in SPC_Output)
                    {
                        OutPutParams = new SqlParameter(SP_Output.ParameterName, SP_Output.SqlDbType);
                        OutPutParams.Direction = ParameterDirection.Output;
                        sqlcom.Parameters.Add(OutPutParams);
                    }

                }
                sqlcom.ExecuteNonQuery();


                foreach (SqlParameter SP_Output in SPC_Output)
                {
                    OutputParametersValues.Add(sqlcom.Parameters[SP_Output.ParameterName.ToString()].Value.ToString());
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sqlcom != null)
                {
                    sqlcom.Connection.Close();
                    sqlcom.Connection.Dispose();
                }
            }
            return OutputParametersValues;
        }

        #endregion
        //-------------------------------Stored Procedure Functionality -------------------------------//
    }
}
