﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TimeAttedance.WebService;

namespace TimeAttedance
{
    public partial class SignUp : Form
    {
        Registery reg = new Registery();
        Main obj = new Main();
        public SignUp()
        {
            InitializeComponent();
            //reg.IsConnected();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            if (obj.ValidateEmail(txtEmail.Text))
            {
                if (txtEmail.Text == "")
                {
                    MessageBox.Show("Enter Email Id", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                    return;
                }
                if (txtPassword.Text == "")
                {
                    MessageBox.Show("Enter Password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPassword.Focus();
                    return;
                }
                Attendance atd = new Attendance();
                string emailId = txtEmail.Text;
                string password = txtPassword.Text;
                //string jsonText = atd.VerifyLogin(emailId, password);

                TimeAttedance.AddUser.AddUser u = new AddUser.AddUser();

                string Id = u.signup(emailId, password, "");

                if (Id != "false")
                {
                    MessageBox.Show("You have signed up successfully! Confirmation email has been sent to your email address", "Time & Attendance", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Email address already exist !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please provide valid Email Address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmail.SelectAll();
            }
        }

    }
}
