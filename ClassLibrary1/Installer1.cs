﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                File.AppendAllText(Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLog.txt", ex.Message + Environment.NewLine);
                File.AppendAllText(Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLog.txt", ex.StackTrace + Environment.NewLine);
            }
        }


        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Uninstall(IDictionary savedState)
        {
            try
            {
                base.Uninstall(savedState);

                //Deleting Files and Folders..
                if (Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima"))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Avaima");
                    foreach (DirectoryInfo subDir in dirInfo.GetDirectories())
                    {
                        foreach (FileInfo file in subDir.GetFiles())
                        {
                            file.Attributes = FileAttributes.Normal;
                            file.Delete();
                        }

                        subDir.Attributes = FileAttributes.Normal;
                        subDir.Delete(true);
                    }

                    foreach (FileInfo file in dirInfo.GetFiles())
                    {
                        file.Attributes = FileAttributes.Normal;
                        file.Delete();
                    }

                    dirInfo.Attributes = FileAttributes.Normal;
                    dirInfo.Delete(true);
                }

                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "msceInter.exe"))
                {
                    File.SetAttributes(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "msceInter.exe", FileAttributes.Normal);
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "msceInter.exe");
                }

                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "Newtonsoft.Json.dll"))
                {
                    File.SetAttributes(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "Newtonsoft.Json.dll", FileAttributes.Normal);
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "Newtonsoft.Json.dll");
                }

                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "login.ico"))
                {
                    File.SetAttributes(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "login.ico", FileAttributes.Normal);
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + @"\" + "login.ico");
                }


                //Deleting From Registry..
                RegistryKey timeAndAttendanceKey;
                timeAndAttendanceKey = Registry.CurrentUser.CreateSubKey("TimeAndAttendance");

                //timeAndAttendanceKey.DeleteSubKey("InstanceId");
                //timeAndAttendanceKey.DeleteSubKey("login");
                //timeAndAttendanceKey.DeleteSubKey("EmailId");
                //timeAndAttendanceKey.DeleteSubKey("Password");
                //timeAndAttendanceKey.DeleteSubKey("UserId");
                //timeAndAttendanceKey.DeleteSubKey("Title");
                //timeAndAttendanceKey.DeleteSubKey("Timezone");
                //timeAndAttendanceKey.DeleteSubKey("AppId");
                //timeAndAttendanceKey.DeleteSubKey("UserName");

                Registry.CurrentUser.DeleteSubKeyTree("TimeAndAttendance");


                //RegistryKey timeAndAttendance = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\TimeAndAttendance");
                //foreach (string key in timeAndAttendance.GetSubKeyNames())
                //{
                //    timeAndAttendance.DeleteSubKey(key);
                //}

                //Registry.CurrentUser.DeleteSubKeyTree(@"SOFTWARE\TimeAndAttendance");

            }
            catch (Exception ex)
            {
                File.AppendAllText(Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLog.txt", ex.Message + Environment.NewLine);
                File.AppendAllText(Environment.GetFolderPath(System.Environment.SpecialFolder.DesktopDirectory) + "\\ErrorLog.txt", ex.StackTrace + Environment.NewLine);
            }

        }
    }
}
